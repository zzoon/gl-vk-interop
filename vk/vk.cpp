/* Mostly taken from piglit.
 */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <array>
#include <vector>
#include "vk.h"

#ifdef VK_PRESENT
#include "vertex_vk.h"
#endif

#define VALIDATION_LAYER

/* static variables */
static VkViewport viewport;
static VkRect2D scissor;

/* static functions */
static VkSampleCountFlagBits
get_num_samples(uint32_t num_samples);

/* Taken from piglit source code */
static char *load_text_file(const char *file_name, unsigned *size)
{
    char *text = NULL;

    FILE *fp;

    fp = fopen(file_name, "r");
    if (fp == NULL) {
        return NULL;
    }

    if (fseek(fp, 0, SEEK_END) == 0) {
        size_t len = (size_t) ftell(fp);
        rewind(fp);

        text = (char *)malloc(len + 1);
        if (text != NULL) {
            size_t total_read = 0;

            do {
                size_t bytes = fread(text + total_read, 1,
                             len - total_read, fp);

                total_read += bytes;
                if (feof(fp)) {
                    break;
                }

                if (ferror(fp)) {
                    free(text);
                    text = NULL;
                    break;
                }
            } while (total_read < len);

            if (text != NULL) {
                text[total_read] = '\0';
            }

            if (size != NULL) {
                *size = total_read;
            }
        }
    }

    fclose(fp);
    return text;
}

#ifdef VALIDATION_LAYER
const std::vector<const char*> validationLayers = {
    "VK_LAYER_KHRONOS_validation"
};

bool checkValidationLayerSupport()
{
    uint32_t layerCount;
    vkEnumerateInstanceLayerProperties(&layerCount, nullptr);

    std::vector<VkLayerProperties> availableLayers(layerCount);
    vkEnumerateInstanceLayerProperties(&layerCount, availableLayers.data());

    printf("layers count:%d\n", layerCount);

    for (const char* layerName : validationLayers) {
        bool layerFound = false;

        for (const auto& layerProperties : availableLayers) {
            if (strcmp(layerName, layerProperties.layerName) == 0) {
                layerFound = true;
                break;
            }
        }

        if (!layerFound) {
            return false;
        }
    }

    return true;
}

static VKAPI_ATTR VkBool32 VKAPI_CALL
debugCallback(VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
              VkDebugUtilsMessageTypeFlagsEXT messageType,
              const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData,
              void* pUserData)
{
    printf( "validation layer: %s\n", pCallbackData->pMessage);

    return VK_FALSE;
}



void populateDebugMessengerCreateInfo(VkDebugUtilsMessengerCreateInfoEXT& createInfo) {
    createInfo = {};
    createInfo.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
    createInfo.messageSeverity = VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT |
                                 VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT |
                                 VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT |
                                 VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
    createInfo.messageType = VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT |
                             VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT |
                             VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;

    createInfo.pfnUserCallback = debugCallback;
}
#endif


/* Vulkan create functions */
static VkInstance
create_instance(const char **extensions, int extension_size)
{
    VkApplicationInfo app_info = {};
    VkInstanceCreateInfo inst_info = {};
    VkInstance inst;

    app_info.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
    app_info.pApplicationName = "vktest";
    app_info.apiVersion = VK_MAKE_VERSION(1, 1, 0);
        app_info.applicationVersion = VK_MAKE_VERSION(1, 1, 0);
          app_info.pEngineName = "No Engine";
          app_info.engineVersion = VK_MAKE_VERSION(1, 1, 0);


    inst_info.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
    inst_info.pApplicationInfo = &app_info;

#ifdef VALIDATION_LAYER
    if (checkValidationLayerSupport()) {
        VkDebugUtilsMessengerCreateInfoEXT debugCreateInfo;

        inst_info.enabledLayerCount = static_cast<uint32_t>(validationLayers.size());
        inst_info.ppEnabledLayerNames = validationLayers.data();

        populateDebugMessengerCreateInfo(debugCreateInfo);
    } else {
        fprintf(stderr, "Not supported for validation layer");
    }
#endif

    if (extensions) {
        inst_info.enabledExtensionCount = extension_size;
        inst_info.ppEnabledExtensionNames = extensions;
    }

    if (vkCreateInstance(&inst_info, 0, &inst) != VK_SUCCESS)
        return VK_NULL_HANDLE;

    return inst;
}

static VkPhysicalDevice
select_physical_device(VkInstance inst)
{
    VkResult res = VK_SUCCESS;
    uint32_t dev_count = 0;
    VkPhysicalDevice *pdevices;
    VkPhysicalDevice pdevice0;

    if ((res =
         vkEnumeratePhysicalDevices(inst, &dev_count, 0)) != VK_SUCCESS)
        return VK_NULL_HANDLE;

    pdevices = (VkPhysicalDevice *) malloc(dev_count * sizeof(VkPhysicalDevice));
    if (vkEnumeratePhysicalDevices(inst, &dev_count, pdevices) !=
        VK_SUCCESS)
        return VK_NULL_HANDLE;

    pdevice0 = pdevices[0];
    free(pdevices);

    return pdevice0;
}

static VkDevice
create_device(VkPhysicalDevice pdev)
{
    const char *deviceExtensions[] = { "VK_KHR_external_memory_fd",
                       "VK_KHR_external_semaphore_fd",
                       VK_KHR_SWAPCHAIN_EXTENSION_NAME };
    VkDeviceQueueCreateInfo dev_queue_info = {};
    VkDeviceCreateInfo dev_info = {};
    VkDevice dev;
    const float queue_priorities = 1.0;

    dev_queue_info.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
    dev_queue_info.queueFamilyIndex = 0;
    dev_queue_info.queueCount = 1;
    dev_queue_info.pQueuePriorities = &queue_priorities;

    dev_info.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
    dev_info.queueCreateInfoCount = 1;
    dev_info.pQueueCreateInfos = &dev_queue_info;
    dev_info.enabledExtensionCount = sizeof(deviceExtensions) / sizeof(char *);
    dev_info.ppEnabledExtensionNames = deviceExtensions;

    if (vkCreateDevice(pdev, &dev_info, 0, &dev) != VK_SUCCESS)
        return VK_NULL_HANDLE;

    return dev;
}

static void
fill_uuid(VkPhysicalDevice pdev, uint8_t *deviceUUID, uint8_t *driverUUID)
{
    VkPhysicalDeviceIDProperties devProp = {};
    VkPhysicalDeviceProperties2 prop2 = {};

    devProp.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_ID_PROPERTIES;

    prop2.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PROPERTIES_2;
    prop2.pNext = &devProp;

    vkGetPhysicalDeviceProperties2(pdev, &prop2);
    memcpy(deviceUUID, devProp.deviceUUID, VK_UUID_SIZE);
    memcpy(driverUUID, devProp.driverUUID, VK_UUID_SIZE);
}

static VkPipelineCache
create_pipeline_cache(VkDevice dev)
{
    VkPipelineCacheCreateInfo pcache_info = {};
    VkPipelineCache pcache;

    pcache_info.sType = VK_STRUCTURE_TYPE_PIPELINE_CACHE_CREATE_INFO;

    if (vkCreatePipelineCache(dev, &pcache_info, 0, &pcache) != VK_SUCCESS)
        return VK_NULL_HANDLE;

    return pcache;
}

static VkCommandPool
create_cmd_pool(VkDevice dev)
{
    VkCommandPoolCreateInfo cmd_pool_info = {};
    VkCommandPool cmd_pool;

    cmd_pool_info.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
    cmd_pool_info.queueFamilyIndex = 0;
    cmd_pool_info.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;

    if (vkCreateCommandPool(dev, &cmd_pool_info, 0, &cmd_pool) != VK_SUCCESS)
        return VK_NULL_HANDLE;

    return cmd_pool;
}

static VkRenderPass
create_renderpass(struct vk_ctx *ctx,
          struct vk_image_props *color_img_props,
          struct vk_image_props *depth_img_props)
{
    unsigned num_attachments = depth_img_props ? 2 : 1;
    VkAttachmentDescription att_dsc[2] = {};
    VkAttachmentReference att_rfc[2] = {};
    VkSubpassDescription subpass_dsc[1] = {};
    VkRenderPassCreateInfo rpass_info = {};

    /* VkAttachmentDescription */
    att_dsc[0].samples = get_num_samples(color_img_props->num_samples);
    att_dsc[0].loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
    att_dsc[0].storeOp = VK_ATTACHMENT_STORE_OP_STORE;
    att_dsc[0].initialLayout = color_img_props->in_layout;
    att_dsc[0].finalLayout = color_img_props->end_layout;
    att_dsc[0].format = color_img_props->format;

    if (depth_img_props) {
        att_dsc[1].samples = get_num_samples(depth_img_props->num_samples);
        att_dsc[1].loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
        att_dsc[1].storeOp = VK_ATTACHMENT_STORE_OP_STORE;
        att_dsc[1].initialLayout = depth_img_props->in_layout;
        att_dsc[1].finalLayout = depth_img_props->end_layout;
        att_dsc[1].format = depth_img_props->format;
    }

    /* VkAttachmentReference */

    att_rfc[0].layout = color_img_props->tiling == VK_IMAGE_TILING_OPTIMAL ?
            VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL : VK_IMAGE_LAYOUT_GENERAL;
    att_rfc[0].attachment = 0;

    if (depth_img_props) {
        att_rfc[1].layout = depth_img_props->tiling == VK_IMAGE_TILING_OPTIMAL ?
                VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL : VK_IMAGE_LAYOUT_GENERAL;
        att_rfc[1].attachment = 1;
    }

    /* VkSubpassDescription */
    subpass_dsc[0].pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
    subpass_dsc[0].colorAttachmentCount = 1;
    subpass_dsc[0].pColorAttachments = &att_rfc[0];
    subpass_dsc[0].pDepthStencilAttachment = depth_img_props ? &att_rfc[1] : NULL;

    /* VkRenderPassCreateInfo */
    rpass_info.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
    rpass_info.attachmentCount = num_attachments;
    rpass_info.pAttachments = att_dsc;
    rpass_info.subpassCount = 1;
    rpass_info.pSubpasses = subpass_dsc;

    VkRenderPass rpass;
    if (vkCreateRenderPass(ctx->dev, &rpass_info, 0, &rpass) != VK_SUCCESS) {
        fprintf(stderr, "Failed to create renderpass.\n");
        rpass = VK_NULL_HANDLE;
    }

    return rpass;
}

static inline VkImageType
get_image_type(uint32_t h, uint32_t d)
{
    if (h == 1)
        return VK_IMAGE_TYPE_1D;

    if (d > 1)
        return VK_IMAGE_TYPE_3D;

    return VK_IMAGE_TYPE_2D;
}

static VkImageViewType
get_image_view_type(struct vk_image_props *props)
{
    VkImageType type = get_image_type(props->h, props->depth);
    switch(type) {
        case VK_IMAGE_TYPE_1D:
            return props->num_layers > 1 ?
                VK_IMAGE_VIEW_TYPE_1D_ARRAY :
                VK_IMAGE_VIEW_TYPE_1D;
        case VK_IMAGE_TYPE_2D:
            if (props->num_layers == 1)
                return VK_IMAGE_VIEW_TYPE_2D;
            if (props->num_layers == 6)
                return VK_IMAGE_VIEW_TYPE_CUBE;
            if (props->num_layers % 6 == 0)
                return VK_IMAGE_VIEW_TYPE_CUBE_ARRAY;
            if (props->num_layers > 1)
                return VK_IMAGE_VIEW_TYPE_2D_ARRAY;
        case VK_IMAGE_TYPE_3D:
            if (props->num_layers == 1)
                return VK_IMAGE_VIEW_TYPE_3D;
            if ((props->num_layers == 1) &&
                (props->num_levels == 1))
                return VK_IMAGE_VIEW_TYPE_2D;
            if ((props->num_levels == 1) &&
                (props->num_layers > 1))
                return VK_IMAGE_VIEW_TYPE_2D_ARRAY;
        default:
            return VK_IMAGE_VIEW_TYPE_2D;
    }
}

static VkImageAspectFlags
get_aspect_from_depth_format(VkFormat depth_format)
{
    switch (depth_format) {
    case VK_FORMAT_D16_UNORM:
    case VK_FORMAT_X8_D24_UNORM_PACK32:
    case VK_FORMAT_D32_SFLOAT:
        return VK_IMAGE_ASPECT_DEPTH_BIT;
    case VK_FORMAT_S8_UINT:
        return VK_IMAGE_ASPECT_STENCIL_BIT;
    case VK_FORMAT_D16_UNORM_S8_UINT:
    case VK_FORMAT_D24_UNORM_S8_UINT:
    case VK_FORMAT_D32_SFLOAT_S8_UINT:
        return VK_IMAGE_ASPECT_DEPTH_BIT | VK_IMAGE_ASPECT_STENCIL_BIT;
    default:
        break;
    }
    fprintf(stderr, "Cannot select a depth or stencil aspect mask for the selected depth format. Default used.\n");
    return VK_IMAGE_ASPECT_DEPTH_BIT;
}

static void
create_framebuffer(struct vk_ctx *ctx,
           struct vk_image_att *color_att,
           struct vk_image_att *depth_att,
           struct vk_renderer *renderer)
{
#ifdef VK_PRESENT
    VkFramebufferCreateInfo fb_info = {};

    fb_info.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
    fb_info.renderPass = renderer->renderpass;
    fb_info.width = color_att->props.w;
    fb_info.height = color_att->props.h;
    fb_info.layers = color_att->props.num_layers ? color_att->props.num_layers : 1;
    fb_info.attachmentCount = 1;
    fb_info.pAttachments = ctx->swapchian_imgviews.data();
#else
    unsigned num_attachments = depth_att ? 2 : 1;
    VkImageSubresourceRange sr = {};
    VkImageViewCreateInfo color_info = {};
    VkImageViewCreateInfo depth_info = {};
    VkFramebufferCreateInfo fb_info = {};
    VkImageView atts[2];
    VkImageViewType view_type = get_image_view_type(&color_att->props);

    if (!color_att->obj.img) {
        fprintf(stderr, "Invalid framebuffer attachment image.\n");
        goto fail;
    }

    /* create image views */

    /* VKImageSubresourceRange */
    sr.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    /* FIXME: if we support mipmaps:
     * If an application wants to use all mip levels
     * or layers in an image after the baseMipLevel
     * or baseArrayLayer, it can set levelCount and
     * layerCount to the special values
     * VK_REMAINING_MIP_LEVELS and
     * VK_REMAINING_ARRAY_LAYERS without knowing the
     * exact number of mip levels or layers.
     * */
    sr.baseMipLevel = 0;
    sr.levelCount = color_att->props.num_levels;
    sr.baseArrayLayer = 0;
    sr.layerCount = color_att->props.num_layers;

    /* color view */
    color_info.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
    color_info.image = color_att->obj.img;
    color_info.viewType = view_type;
    //color_info.viewType = VK_IMAGE_VIEW_TYPE_2D;
    color_info.format = color_att->props.format;
    color_info.subresourceRange = sr;

    if (vkCreateImageView(ctx->dev, &color_info, 0, &atts[0]) != VK_SUCCESS) {
        fprintf(stderr, "Failed to create color image view for framebuffer.\n");
        vk_destroy_ext_image(ctx, &color_att->obj);
        goto fail;
    }

    if (depth_att) {
        /* depth view */
        sr.aspectMask = get_aspect_from_depth_format(depth_att->props.format);
        sr.baseMipLevel = 0;
        sr.levelCount = depth_att->props.num_levels ? depth_att->props.num_levels : 1;
        sr.baseArrayLayer = 0;
        sr.layerCount = depth_att->props.num_layers ? depth_att->props.num_layers : 1;

        depth_info.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
        depth_info.image = depth_att->obj.img;
        /* FIXME: depth attachment might have other image view types as well */
        depth_info.viewType = depth_att->props.num_layers > 1 ? VK_IMAGE_VIEW_TYPE_2D_ARRAY : VK_IMAGE_VIEW_TYPE_2D;
        depth_info.viewType = VK_IMAGE_VIEW_TYPE_2D;
        depth_info.format = depth_att->props.format;
        depth_info.subresourceRange = sr;

        if (vkCreateImageView(ctx->dev, &depth_info, 0, &atts[1]) != VK_SUCCESS) {
            fprintf(stderr, "Failed to create depth image view for framebuffer.\n");
            vk_destroy_ext_image(ctx, &depth_att->obj);
            goto fail;
        }
    }

    fb_info.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
    fb_info.renderPass = renderer->renderpass;
    fb_info.width = color_att->props.w;
    fb_info.height = color_att->props.h;
    fb_info.layers = color_att->props.num_layers ? color_att->props.num_layers : 1;
    fb_info.attachmentCount = num_attachments;
    fb_info.pAttachments = atts;
#endif

    if (vkCreateFramebuffer(ctx->dev, &fb_info, 0, &renderer->fb) != VK_SUCCESS)
        goto fail;

    return;

fail:
    fprintf(stderr, "Failed to create framebuffer.\n");
    renderer->fb = VK_NULL_HANDLE;
}

static VkShaderModule
create_shader_module(struct vk_ctx *ctx,
             const char *src,
             unsigned int size)
{
    VkShaderModuleCreateInfo sm_info = {};
    VkShaderModule sm;

    /* VkShaderModuleCreateInfo */
    sm_info.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
    sm_info.codeSize = size;
    sm_info.pCode = (unsigned *)src;

    if (vkCreateShaderModule(ctx->dev, &sm_info, 0, &sm) != VK_SUCCESS) {
        fprintf(stderr, "Failed to create shader module.\n");
        sm = VK_NULL_HANDLE;
    }

    return sm;
}

static void
create_pipeline(struct vk_ctx *ctx,
        uint32_t width,
        uint32_t height,
        uint32_t num_samples,
        struct vk_renderer *renderer)
{
    VkPipelineColorBlendAttachmentState cb_att_state[1] = {};
    VkPipelineVertexInputStateCreateInfo vert_input_info = {};
    VkPipelineInputAssemblyStateCreateInfo asm_info = {};
    VkPipelineViewportStateCreateInfo viewport_info = {};
    VkPipelineRasterizationStateCreateInfo rs_info = {};
    VkPipelineMultisampleStateCreateInfo ms_info = {};
    VkPipelineDepthStencilStateCreateInfo ds_info = {};
    VkPipelineColorBlendStateCreateInfo cb_info = {};
    VkPipelineShaderStageCreateInfo sdr_stages[2] = {};
    VkPipelineLayoutCreateInfo layout_info = {};
    VkGraphicsPipelineCreateInfo pipeline_info = {};
    VkPushConstantRange pc_range[1] = {};

    VkPrimitiveTopology topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP;
    VkStencilOpState front = {};
    VkStencilOpState back = {};
    int i;
    VkPipelineLayout pipeline_layout;

#ifdef VK_PRESENT
    VkVertexInputBindingDescription vert_bind_dsc[1] = {};
    VkVertexInputAttributeDescription vert_att_dsc[3] = {};
    VkFormat format;
    VkFormatProperties fmt_props;
    uint32_t stride = sizeof(Vertex);

    /* VkVertexInputBindingDescription */
    vert_bind_dsc[0].binding = 0;
    vert_bind_dsc[0].stride = stride;
    vert_bind_dsc[0].inputRate = VK_VERTEX_INPUT_RATE_VERTEX;

    /* format of vertex attributes:
     * we have 2D vectors so we need a RG format:
     * R for x, G for y
     * the stride (distance between 2 consecutive elements)
     * must be 8 because we use 32 bit floats and
     * 32bits = 8bytes */
    format = VK_FORMAT_R32G32_SFLOAT;
    vkGetPhysicalDeviceFormatProperties(ctx->pdev, format, &fmt_props);
    assert(fmt_props.bufferFeatures & VK_FORMAT_FEATURE_VERTEX_BUFFER_BIT);

    /* VkVertexInputAttributeDescription */
    vert_att_dsc[0].binding = 0;
    vert_att_dsc[0].location = 0;
    vert_att_dsc[0].format = VK_FORMAT_R32G32_SFLOAT;
    vert_att_dsc[0].offset = offsetof(Vertex, pos);;

    vert_att_dsc[1].binding = 0;
    vert_att_dsc[1].location = 1;
    vert_att_dsc[1].format = VK_FORMAT_R32G32B32A32_SFLOAT;
    vert_att_dsc[1].offset = offsetof(Vertex, color);;

    vert_att_dsc[2].binding = 0;
    vert_att_dsc[2].location = 2;
    vert_att_dsc[2].format = VK_FORMAT_R32G32_SFLOAT;
    vert_att_dsc[2].offset = offsetof(Vertex, uv);;

    /* VkPipelineVertexInputStateCreateInfo */
    vert_input_info.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
    vert_input_info.vertexBindingDescriptionCount = 1;
    vert_input_info.pVertexBindingDescriptions = vert_bind_dsc;
    vert_input_info.vertexAttributeDescriptionCount = 3;
    vert_input_info.pVertexAttributeDescriptions = vert_att_dsc;
#else
    vert_input_info.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
#endif

    /* VkPipelineInputAssemblyStateCreateInfo */
    asm_info.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
    asm_info.topology = topology;
    asm_info.primitiveRestartEnable = false;

    /* VkViewport */
    viewport.x = viewport.y = 0;
    viewport.width = width;
    viewport.height = height;
    viewport.minDepth = 0;
    viewport.maxDepth = 1;

    /* VkRect2D scissor */
    scissor.offset.x = scissor.offset.y = 0;
    scissor.extent.width = width;
    scissor.extent.height = height;

    /* VkPipelineViewportStateCreateInfo */
    viewport_info.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
    viewport_info.viewportCount = 1;
    viewport_info.pViewports = &viewport;
    viewport_info.scissorCount = 1;
    viewport_info.pScissors = &scissor;

    /* VkPipelineRasterizationStateCreateInfo */
    rs_info.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
    rs_info.polygonMode = VK_POLYGON_MODE_FILL;
    rs_info.cullMode = VK_CULL_MODE_NONE;
    rs_info.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;

    /* VkPipelineMultisampleStateCreateInfo */
    ms_info.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
    ms_info.rasterizationSamples = (VkSampleCountFlagBits) num_samples;

    /* VkStencilOpState */
    /* The default values for ES are taken by Topi Pohjolainen's code */
    /* defaults in OpenGL ES 3.1 */
    front.compareMask = ~0;
    front.writeMask = ~0;
    front.reference = 0;

    back.compareMask = ~0;
    back.writeMask = ~0;
    back.reference = 0;

    /* VkPipelineDepthStencilStateCreateInfo */
    ds_info.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
    ds_info.front = front;
    ds_info.back = back;
    /* defaults in OpenGL ES 3.1 */
    ds_info.minDepthBounds = 0;
    ds_info.maxDepthBounds = 1;

    /* VkPipelineColorBlendAttachmentState */
    cb_att_state[0].colorWriteMask = (VK_COLOR_COMPONENT_R_BIT |
                      VK_COLOR_COMPONENT_G_BIT |
                      VK_COLOR_COMPONENT_B_BIT |
                      VK_COLOR_COMPONENT_A_BIT);

    /* VkPipelineColorBlendStateCreateInfo */
    cb_info.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
    cb_info.attachmentCount = 1;
    cb_info.pAttachments = cb_att_state;
    /* default in ES 3.1 */
    for (i = 0; i < 4; i++) {
        cb_info.blendConstants[i] = 0.0f;
    }

    /* VkPipelineShaderStageCreateInfo */
    sdr_stages[0].sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
    sdr_stages[0].stage = VK_SHADER_STAGE_VERTEX_BIT;
    sdr_stages[0].module = renderer->vs;
    sdr_stages[0].pName = "main";

    sdr_stages[1].sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
    sdr_stages[1].stage = VK_SHADER_STAGE_FRAGMENT_BIT;
    sdr_stages[1].module = renderer->fs;
    sdr_stages[1].pName = "main";

    /* VkPushConstantRange */
    pc_range[0].stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;
    pc_range[0].size = sizeof (struct vk_dims); /* w, h */

    /* VkPipelineLayoutCreateInfo */
    layout_info.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
    if (ctx->swapchain_setup) {
        layout_info.setLayoutCount = 1;
        layout_info.pSetLayouts = &ctx->desc_layout.desc_set_layout;
    }
    layout_info.pushConstantRangeCount = 1;
    layout_info.pPushConstantRanges = pc_range;

    if (vkCreatePipelineLayout(ctx->dev, &layout_info, 0, &pipeline_layout) != VK_SUCCESS) {
        fprintf(stderr, "Failed to create pipeline layout\n");
        renderer->pipeline = VK_NULL_HANDLE;
        return;
    }

    renderer->pipeline_layout = pipeline_layout;

    VkDynamicState dynamicStates[] = {
        VK_DYNAMIC_STATE_VIEWPORT,
        VK_DYNAMIC_STATE_LINE_WIDTH
    };

    VkPipelineDynamicStateCreateInfo dynamicState = {};
    dynamicState.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
    dynamicState.dynamicStateCount = 2;
    dynamicState.pDynamicStates = dynamicStates;

    /* VkGraphicsPipelineCreateInfo */
    pipeline_info.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
    pipeline_info.layout = pipeline_layout;
    pipeline_info.renderPass = renderer->renderpass;
    pipeline_info.pVertexInputState = &vert_input_info;
    pipeline_info.pInputAssemblyState = &asm_info;
    pipeline_info.pViewportState = &viewport_info;
    pipeline_info.pRasterizationState = &rs_info;
    pipeline_info.pMultisampleState = &ms_info;
    pipeline_info.pDepthStencilState = &ds_info;
    pipeline_info.pColorBlendState = &cb_info;
    pipeline_info.stageCount = 2;
    pipeline_info.pStages = sdr_stages;
    pipeline_info.pDynamicState = &dynamicState;

    if (vkCreateGraphicsPipelines(ctx->dev, ctx->cache, 1,
                      &pipeline_info, 0, &renderer->pipeline) !=
            VK_SUCCESS) {
        fprintf(stderr, "Failed to create graphics pipeline.\n");
        renderer->pipeline = VK_NULL_HANDLE;
    }
}

static VkCommandBuffer
create_cmd_buf(VkDevice dev, VkCommandPool cmd_pool)
{
    VkCommandBuffer cmd_buf;
    VkCommandBufferAllocateInfo alloc_info = {};

    alloc_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    alloc_info.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    alloc_info.commandBufferCount = 1;
    alloc_info.commandPool = cmd_pool;

    if (vkAllocateCommandBuffers(dev, &alloc_info, &cmd_buf) != VK_SUCCESS)
        return VK_NULL_HANDLE;

    return cmd_buf;
}

static uint32_t
get_memory_type_idx(VkPhysicalDevice pdev,
            const VkMemoryRequirements *mem_reqs,
            VkMemoryPropertyFlags prop_flags)
{
    VkPhysicalDeviceMemoryProperties pdev_mem_props;
    uint32_t i;

    vkGetPhysicalDeviceMemoryProperties(pdev, &pdev_mem_props);

    for (i = 0; i < pdev_mem_props.memoryTypeCount; i++) {
        const VkMemoryType *type = &pdev_mem_props.memoryTypes[i];

        if ((mem_reqs->memoryTypeBits & (1 << i)) &&
            (type->propertyFlags & prop_flags) == prop_flags) {
            return i;
            break;
        }
    }
    return UINT32_MAX;
}

static VkSampleCountFlagBits
get_num_samples(uint32_t num_samples)
{
    switch(num_samples) {
    case 64:
        return VK_SAMPLE_COUNT_64_BIT;
    case 32:
        return VK_SAMPLE_COUNT_32_BIT;
    case 16:
        return VK_SAMPLE_COUNT_16_BIT;
    case 8:
        return VK_SAMPLE_COUNT_8_BIT;
    case 4:
        return VK_SAMPLE_COUNT_4_BIT;
    case 2:
        return VK_SAMPLE_COUNT_2_BIT;
    case 1:
        break;
    default:
        fprintf(stderr, "Invalid number of samples in VkSampleCountFlagBits. Using one sample.\n");
        break;
    }
    return VK_SAMPLE_COUNT_1_BIT;
}

static VkDeviceMemory
alloc_memory(struct vk_ctx *ctx,
         const VkMemoryRequirements *mem_reqs,
         VkMemoryPropertyFlags prop_flags)
{
    VkExportMemoryAllocateInfo exp_mem_info = {};
    VkMemoryAllocateInfo mem_alloc_info = {};
    VkDeviceMemory mem;

    exp_mem_info.sType = VK_STRUCTURE_TYPE_EXPORT_MEMORY_ALLOCATE_INFO;
    exp_mem_info.handleTypes =
        VK_EXTERNAL_MEMORY_HANDLE_TYPE_OPAQUE_FD_BIT;

    mem_alloc_info.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
    mem_alloc_info.pNext = &exp_mem_info;
    mem_alloc_info.allocationSize = mem_reqs->size;
    mem_alloc_info.memoryTypeIndex =
        get_memory_type_idx(ctx->pdev, mem_reqs, prop_flags);

    if (mem_alloc_info.memoryTypeIndex == UINT32_MAX) {
        fprintf(stderr, "No suitable memory type index found.\n");
        return VK_NULL_HANDLE;
    }

    if (vkAllocateMemory(ctx->dev, &mem_alloc_info, 0, &mem) !=
        VK_SUCCESS)
        return VK_NULL_HANDLE;

    return mem;
}

static bool
alloc_image_memory(struct vk_ctx *ctx, struct vk_image_obj *img_obj)
{
    VkImageMemoryRequirementsInfo2 req_info2 = {};
    VkMemoryRequirements2 mem_reqs2 = {};

    /* VkImageMemoryRequirementsInfo2 */
    req_info2.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_REQUIREMENTS_INFO_2;
    req_info2.image = img_obj->img;

    /* VkMemoryRequirements2 */
    mem_reqs2.sType = VK_STRUCTURE_TYPE_MEMORY_REQUIREMENTS_2;

    vkGetImageMemoryRequirements2(ctx->dev, &req_info2, &mem_reqs2);
    img_obj->mem = alloc_memory(ctx,
                    &mem_reqs2.memoryRequirements,
                    (mem_reqs2.memoryRequirements.memoryTypeBits) &
                    VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);

    img_obj->mem_sz = mem_reqs2.memoryRequirements.size;
    if (img_obj->mem == VK_NULL_HANDLE) {
        fprintf(stderr, "Failed to allocate image memory.\n");
        return false;
    }

    if (vkBindImageMemory(ctx->dev, img_obj->img, img_obj->mem, 0) != VK_SUCCESS) {
        fprintf(stderr, "Failed to bind image memory.\n");
        return false;
    }

    return true;
}

static bool
are_props_supported(struct vk_ctx *ctx, struct vk_image_props *props)
{
    VkPhysicalDeviceExternalImageFormatInfo ext_img_fmt_info = {};
    VkPhysicalDeviceImageFormatInfo2 img_fmt_info = {};
    VkExternalImageFormatProperties ext_img_fmt_props = {};
    VkImageFormatProperties2 img_fmt_props = {};

    VkExternalMemoryFeatureFlagBits feature_flags =
        VK_EXTERNAL_MEMORY_FEATURE_EXPORTABLE_BIT;
    VkExternalMemoryHandleTypeFlagBits handle_type =
        VK_EXTERNAL_MEMORY_HANDLE_TYPE_OPAQUE_FD_BIT;

    ext_img_fmt_info.sType =
        VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_EXTERNAL_IMAGE_FORMAT_INFO;
    ext_img_fmt_info.handleType = handle_type;

    img_fmt_info.sType =
        VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_IMAGE_FORMAT_INFO_2;
    img_fmt_info.pNext = &ext_img_fmt_info;
    img_fmt_info.format = props->format;
    img_fmt_info.type = get_image_type(props->h, props->depth);
    img_fmt_info.tiling = props->tiling;
    img_fmt_info.usage = props->usage;

    ext_img_fmt_props.sType =
        VK_STRUCTURE_TYPE_EXTERNAL_IMAGE_FORMAT_PROPERTIES;

    img_fmt_props.sType = VK_STRUCTURE_TYPE_IMAGE_FORMAT_PROPERTIES_2;
    img_fmt_props.pNext = &ext_img_fmt_props;

    if (vkGetPhysicalDeviceImageFormatProperties2
        (ctx->pdev, &img_fmt_info, &img_fmt_props) != VK_SUCCESS) {
        fprintf(stderr,
            "Unsupported Vulkan format properties.\n");
        return false;
    }

    if (!(ext_img_fmt_props.externalMemoryProperties.externalMemoryFeatures & feature_flags)) {
        fprintf(stderr, "Unsupported Vulkan external memory features.\n");
        return false;
    }

    return true;
}

/* Vulkan test visible functions */

bool
vk_init_ctx(struct vk_ctx *ctx, const char **extensions, int extension_size)
{
    if ((ctx->inst = create_instance(extensions, extension_size)) == VK_NULL_HANDLE) {
        fprintf(stderr, "Failed to create Vulkan instance.\n");
        goto fail;
    }

    if ((ctx->pdev = select_physical_device(ctx->inst)) == VK_NULL_HANDLE) {
        fprintf(stderr, "Failed to find suitable physical device.\n");
        goto fail;
    }

    if ((ctx->dev = create_device(ctx->pdev)) == VK_NULL_HANDLE) {
        fprintf(stderr, "Failed to create Vulkan device.\n");
        goto fail;
    }

    fill_uuid(ctx->pdev, ctx->deviceUUID, ctx->driverUUID);
    return true;

fail:
    vk_cleanup_ctx(ctx);
    return false;
}

bool
vk_init_ctx_for_rendering(struct vk_ctx *ctx, const char **extensions, int extension_size)
{
    if (!vk_init_ctx(ctx, extensions, extension_size)) {
        fprintf(stderr, "Failed to initialize Vulkan.\n");
        goto fail;
    }

    if ((ctx->cache = create_pipeline_cache(ctx->dev)) == VK_NULL_HANDLE) {
        fprintf(stderr, "Failed to create pipeline cache.\n");
        goto fail;
    }

    if ((ctx->cmd_pool = create_cmd_pool(ctx->dev)) == VK_NULL_HANDLE) {
        fprintf(stderr, "Failed to create command pool.\n");
        goto fail;
    }

    if ((ctx->cmd_buf = create_cmd_buf(ctx->dev, ctx->cmd_pool)) ==
            VK_NULL_HANDLE) {
        fprintf(stderr, "Failed to create command buffer.\n");
        goto fail;
    }

    vkGetDeviceQueue(ctx->dev, 0, 0, &ctx->queue);
    if (!ctx->queue) {
        fprintf(stderr, "Failed to get command queue.\n");
        goto fail;
    }

    return true;

fail:
    vk_cleanup_ctx(ctx);
    return false;
}

void
vk_cleanup_ctx(struct vk_ctx *ctx)
{
    if (ctx->cmd_buf != VK_NULL_HANDLE)
        vkFreeCommandBuffers(ctx->dev, ctx->cmd_pool, 1, &ctx->cmd_buf);

    if (ctx->cmd_pool != VK_NULL_HANDLE)
        vkDestroyCommandPool(ctx->dev, ctx->cmd_pool, 0);

    if (ctx->cache != VK_NULL_HANDLE)
        vkDestroyPipelineCache(ctx->dev, ctx->cache, 0);

    if (ctx->dev != VK_NULL_HANDLE)
        vkDestroyDevice(ctx->dev, 0);

    if (ctx->inst != VK_NULL_HANDLE)
        vkDestroyInstance(ctx->inst, 0);
}

bool
vk_create_sampler(struct vk_ctx *ctx, VkSampler *sampler)
{
    VkSamplerCreateInfo samplerInfo = {};
    samplerInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
    samplerInfo.magFilter = VK_FILTER_LINEAR;
    samplerInfo.minFilter = VK_FILTER_LINEAR;
    samplerInfo.addressModeU = VK_SAMPLER_ADDRESS_MODE_REPEAT;
    samplerInfo.addressModeV = VK_SAMPLER_ADDRESS_MODE_REPEAT;
    samplerInfo.addressModeW = VK_SAMPLER_ADDRESS_MODE_REPEAT;
    samplerInfo.anisotropyEnable = VK_FALSE;
    samplerInfo.maxAnisotropy = 16;
    samplerInfo.borderColor = VK_BORDER_COLOR_INT_OPAQUE_BLACK;
    samplerInfo.unnormalizedCoordinates = VK_FALSE;
    samplerInfo.compareEnable = VK_FALSE;
    samplerInfo.compareOp = VK_COMPARE_OP_ALWAYS;
    samplerInfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
    samplerInfo.mipLodBias = 0.0f;
    samplerInfo.minLod = 0.0f;
    samplerInfo.maxLod = 0.0f;

    if (vkCreateSampler(ctx->dev, &samplerInfo, nullptr, sampler) != VK_SUCCESS) {
        fprintf(stderr, "failed to create texture sampler!\n");
        return false;
    }

    return true;
}

bool
vk_create_ext_image(struct vk_ctx *ctx,
            struct vk_image_props *props, struct vk_image_obj *img)
{
    VkExternalMemoryImageCreateInfo ext_img_info = {};
    VkImageCreateInfo img_info = {};

    ext_img_info.sType = VK_STRUCTURE_TYPE_EXTERNAL_MEMORY_IMAGE_CREATE_INFO;
    ext_img_info.handleTypes = VK_EXTERNAL_MEMORY_HANDLE_TYPE_OPAQUE_FD_BIT_KHR;

    img_info.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
    img_info.pNext = &ext_img_info;
    img_info.imageType = get_image_type(props->h, props->depth);
    img_info.format = props->format;
    img_info.extent.width = props->w;
    img_info.extent.height = props->h;
    img_info.extent.depth = props->depth;
    img_info.mipLevels = props->num_levels ? props->num_levels : 1;
    img_info.arrayLayers = props->num_layers ? props->num_layers : VK_SAMPLE_COUNT_1_BIT;
    img_info.samples = get_num_samples(props->num_samples);
    img_info.tiling = props->tiling;
    img_info.usage = props->usage;
    img_info.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;

    if (vkCreateImage(ctx->dev, &img_info, 0, &img->img) != VK_SUCCESS)
        goto fail;

    if(!alloc_image_memory(ctx, img))
        goto fail;

    return true;

fail:
    fprintf(stderr, "Failed to create external image.\n");
    vk_destroy_ext_image(ctx, img);
    img->img = VK_NULL_HANDLE;
    img->mem = VK_NULL_HANDLE;
    return false;
}

void
vk_destroy_ext_image(struct vk_ctx *ctx, struct vk_image_obj *img_obj)
{
    if (img_obj->img != VK_NULL_HANDLE)
        vkDestroyImage(ctx->dev, img_obj->img, 0);

    if (img_obj->mem != VK_NULL_HANDLE)
        vkFreeMemory(ctx->dev, img_obj->mem, 0);
}

bool
vk_fill_ext_image_props(struct vk_ctx *ctx,
            uint32_t w,
            uint32_t h,
            uint32_t d,
            uint32_t num_samples,
            uint32_t num_levels,
            uint32_t num_layers,
            VkFormat format,
            VkImageTiling tiling,
            VkImageUsageFlags usage,
            VkImageLayout in_layout,
            VkImageLayout end_layout,
            struct vk_image_props *props)
{
    props->w = w;
    props->h = h;
    props->depth = d;

    props->num_samples = num_samples;
    props->num_levels = num_levels;
    props->num_layers = num_layers;

    props->format = format;
    props->usage = usage;
    props->tiling = tiling;

    props->in_layout = in_layout;
    props->end_layout = end_layout;

    if (!are_props_supported(ctx, props))
        return false;

    return true;
}

bool
vk_create_image_view(struct vk_ctx *ctx, VkImage img, VkImageView *view)
{
    VkImageViewCreateInfo color_info = {};
    color_info.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
    color_info.image = img;
    color_info.viewType = VK_IMAGE_VIEW_TYPE_2D;
    color_info.format = VK_FORMAT_B8G8R8A8_UNORM;
    color_info.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    color_info.subresourceRange.baseMipLevel = 0;
    color_info.subresourceRange.levelCount = 1;
    color_info.subresourceRange.baseArrayLayer = 0;
    color_info.subresourceRange.layerCount = 1;

    if (vkCreateImageView(ctx->dev, &color_info, 0, view) != VK_SUCCESS) {
        fprintf(stderr, "Failed to create color image view for framebuffer.\n");
        return false;
    }

    return true;
}

bool
vk_create_renderer(struct vk_ctx *ctx,
           const char *vs_path,
           const char *fs_path,
           struct vk_image_att *color_att,
           struct vk_image_att *depth_att,
           struct vk_renderer *renderer)
{
    const char *shader_src = NULL;
    unsigned shader_size;

    renderer->renderpass = create_renderpass(ctx,
                                             &color_att->props,
                                             depth_att ? &depth_att->props : NULL);
    if (renderer->renderpass == VK_NULL_HANDLE)
        goto fail;

    create_framebuffer(ctx, color_att, depth_att, renderer);
    if (renderer->fb == VK_NULL_HANDLE)
        goto fail;

    shader_src = load_text_file(vs_path, &shader_size);

    renderer->vs = create_shader_module(ctx, shader_src, shader_size);
    if (renderer->vs == VK_NULL_HANDLE)
        goto fail;

    free((void *)shader_src);

    shader_src = load_text_file(fs_path, &shader_size);

    renderer->fs = create_shader_module(ctx, shader_src, shader_size);
    if (renderer->fs == VK_NULL_HANDLE)
        goto fail;

    free((void *)shader_src);

    create_pipeline(ctx, color_att->props.w, color_att->props.h,
            color_att->props.num_samples, renderer);
    if (renderer->pipeline == VK_NULL_HANDLE)
        goto fail;

    return true;

fail:
    fprintf(stderr, "Failed to create graphics pipeline.\n");
    vk_destroy_renderer(ctx, renderer);
    return false;
}

void
vk_destroy_renderer(struct vk_ctx *ctx,
            struct vk_renderer *renderer)
{
    if (renderer->renderpass != VK_NULL_HANDLE)
        vkDestroyRenderPass(ctx->dev, renderer->renderpass, 0);

    if (renderer->vs != VK_NULL_HANDLE)
        vkDestroyShaderModule(ctx->dev, renderer->vs, 0);

    if (renderer->fs != VK_NULL_HANDLE)
        vkDestroyShaderModule(ctx->dev, renderer->fs, 0);

    if (renderer->pipeline != VK_NULL_HANDLE)
        vkDestroyPipeline(ctx->dev, renderer->pipeline, 0);

    if (renderer->fb != VK_NULL_HANDLE)
        vkDestroyFramebuffer(ctx->dev, renderer->fb, 0);

    if (renderer->pipeline_layout != VK_NULL_HANDLE)
        vkDestroyPipelineLayout(ctx->dev, renderer->pipeline_layout, 0);
}

bool
vk_create_buffer(struct vk_ctx *ctx,
         uint32_t sz,
         VkBufferUsageFlags usage,
         struct vk_buf *bo)
{
    VkBufferCreateInfo buf_info = {};
    VkMemoryRequirements mem_reqs;

    bo->mem = VK_NULL_HANDLE;
    bo->buf = VK_NULL_HANDLE;

    /* VkBufferCreateInfo */
    buf_info.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
    buf_info.size = sz;
    buf_info.usage = usage;

    if (vkCreateBuffer(ctx->dev, &buf_info, 0, &bo->buf) != VK_SUCCESS)
        goto fail;

    /* allocate buffer */
    vkGetBufferMemoryRequirements(ctx->dev, bo->buf, &mem_reqs);
    /* FIXME:
     * VK_MEMORY_PROPERTY_HOST_COHERENT_BIT bit specifies that the
     * host cache management commands vkFlushMappedMemoryRanges and
     * vkInvalidateMappedMemoryRanges are not needed to flush host
     * writes to the device or make device writes visible to the
     * host, respectively. */
    bo->mem = alloc_memory(ctx, &mem_reqs, VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);
    if (bo->mem == VK_NULL_HANDLE)
        goto fail;

    if (vkBindBufferMemory(ctx->dev, bo->buf, bo->mem, 0) != VK_SUCCESS) {
        fprintf(stderr, "Failed to bind buffer memory.\n");
        goto fail;
    }

    bo->size = sz;

    return true;

fail:
    fprintf(stderr, "Failed to allocate buffer.\n");
    vk_destroy_buffer(ctx, bo);
    return false;
}

bool
vk_update_buffer_data(struct vk_ctx *ctx,
              void *data,
              uint32_t data_sz,
              struct vk_buf *bo)
{
    void *map;

    if (vkMapMemory(ctx->dev, bo->mem, 0, data_sz, 0, &map) != VK_SUCCESS) {
        fprintf(stderr, "Failed to map buffer memory.\n");
        goto fail;
    }

    memcpy(map, data, data_sz);

    vkUnmapMemory(ctx->dev, bo->mem);
    return true;

fail:
    fprintf(stderr, "Failed to update buffer data. Destroying the buffer.\n");
    vk_destroy_buffer(ctx, bo);

    return false;
}

void
vk_destroy_buffer(struct vk_ctx *ctx,
          struct vk_buf *bo)
{
    if (bo->mem != VK_NULL_HANDLE)
        vkFreeMemory(ctx->dev, bo->mem, 0);
    if (bo->buf != VK_NULL_HANDLE)
        vkDestroyBuffer(ctx->dev, bo->buf, 0);

    bo->buf = VK_NULL_HANDLE;
    bo->mem = VK_NULL_HANDLE;
}

void
vk_build_draw_cmd(struct vk_ctx *ctx,
    struct vk_buf *vbo,
    struct vk_buf *ibo,
    struct vk_dims *dims,
    struct vk_renderer *renderer,
    float *vk_fb_color,
    uint32_t vk_fb_color_count,
    uint32_t w, uint32_t h)
{
    VkCommandBufferBeginInfo cmd_begin_info = {};
    VkRenderPassBeginInfo rp_begin_info = {};
    VkRect2D rp_area = {};
    VkClearValue clear_values[2] = {};
    VkDeviceSize dev_sz = 0;

    assert(vk_fb_color_count == 4);

    /* VkCommandBufferBeginInfo */
    cmd_begin_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;

    /* VkRect2D render area */
    rp_area.extent.width = w;
    rp_area.extent.height = h;

    /* VkClearValue */
    clear_values[0].color.float32[0] = vk_fb_color[0]; /* red */
    clear_values[0].color.float32[1] = vk_fb_color[1]; /* green */
    clear_values[0].color.float32[2] = vk_fb_color[2]; /* blue */
    clear_values[0].color.float32[3] = vk_fb_color[3]; /* alpha */

    clear_values[1].depthStencil.depth = 1.0;

    /* VkRenderPassBeginInfo */
    rp_begin_info.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
    rp_begin_info.renderPass = renderer->renderpass;
    rp_begin_info.framebuffer = renderer->fb;
    rp_begin_info.renderArea = rp_area;
    rp_begin_info.clearValueCount = 2;
    rp_begin_info.pClearValues = clear_values;

    vkBeginCommandBuffer(ctx->cmd_buf, &cmd_begin_info);
    vkCmdBeginRenderPass(ctx->cmd_buf, &rp_begin_info, VK_SUBPASS_CONTENTS_INLINE);

    vkCmdSetViewport(ctx->cmd_buf, 0, 1, &viewport);
    vkCmdSetScissor(ctx->cmd_buf, 0, 1, &scissor);

    if (dims) {
        vkCmdPushConstants(ctx->cmd_buf,
                   renderer->pipeline_layout,
                   VK_SHADER_STAGE_FRAGMENT_BIT,
                   0, sizeof (struct vk_dims),
                   dims);
    }

    if (vbo) {
        vkCmdBindVertexBuffers(ctx->cmd_buf, 0, 1, &vbo->buf, &dev_sz);
    }
    if (ibo) {
        vkCmdBindIndexBuffer(ctx->cmd_buf, ibo->buf, 0, VK_INDEX_TYPE_UINT16);
    }
    if (ctx->desc_layout.desc_sets.size() > 0) {
        vkCmdBindDescriptorSets(ctx->cmd_buf, VK_PIPELINE_BIND_POINT_GRAPHICS, renderer->pipeline_layout, 0, 1, ctx->desc_layout.desc_sets.data(), 0, nullptr);
    }
    vkCmdBindPipeline(ctx->cmd_buf, VK_PIPELINE_BIND_POINT_GRAPHICS, renderer->pipeline);

    if (ibo) {
        vkCmdDrawIndexed(ctx->cmd_buf, 6, 1, 0, 0, 0);
    } else {
        vkCmdDraw(ctx->cmd_buf, 4, 1, 0, 0);
    }

    vkCmdEndRenderPass(ctx->cmd_buf);
    vkEndCommandBuffer(ctx->cmd_buf);
}

void
vk_acquire(struct vk_ctx *ctx, vk_semaphores *sem, unsigned* img_idx)
{
    vkAcquireNextImageKHR(ctx->dev, ctx->swapchain, UINT64_MAX, sem->vk_acquire_done, NULL, img_idx);
}

void
vk_submit(struct vk_ctx *ctx, vk_semaphores *sem)
{
    VkSubmitInfo submit_info = {};
    VkPipelineStageFlags stage_flags = VK_PIPELINE_STAGE_ALL_GRAPHICS_BIT;

    submit_info.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    submit_info.commandBufferCount = 1;
    submit_info.pCommandBuffers = &ctx->cmd_buf;

    submit_info.pWaitDstStageMask = &stage_flags;
    submit_info.waitSemaphoreCount = 1;
    submit_info.pWaitSemaphores = &sem->vk_frame_ready;

    submit_info.signalSemaphoreCount = 1;
    submit_info.pSignalSemaphores = &sem->vk_frame_done;

    if (vkQueueSubmit(ctx->queue, 1, &submit_info, VK_NULL_HANDLE) != VK_SUCCESS) {
        fprintf(stderr, "Failed to submit queue.\n");
    }
}

void
vk_present(struct vk_ctx *ctx, vk_semaphores *sem, unsigned img_idx)
{
    VkPresentInfoKHR presentInfo = {};
    presentInfo.sType              = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
    presentInfo.swapchainCount     = 1;
    presentInfo.pSwapchains        = &ctx->swapchain;
    presentInfo.pImageIndices      = &img_idx;

    if (vkQueuePresentKHR(ctx->queue, &presentInfo) != VK_SUCCESS) {
        fprintf(stderr, "Failed to present.\n");
    }
}

bool
vk_create_semaphores(struct vk_ctx *ctx,
             struct vk_semaphores *semaphores)
{
    VkSemaphoreCreateInfo sema_info = {};
    VkExportSemaphoreCreateInfo exp_sema_info = {};

    /* VkExportSemaphoreCreateInfo */
    exp_sema_info.sType = VK_STRUCTURE_TYPE_EXPORT_SEMAPHORE_CREATE_INFO;
    exp_sema_info.handleTypes = VK_EXTERNAL_SEMAPHORE_HANDLE_TYPE_OPAQUE_FD_BIT;

    /* VkSemaphoreCreateInfo */
    sema_info.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
    sema_info.pNext = &exp_sema_info;

    if (vkCreateSemaphore(ctx->dev, &sema_info, 0, &semaphores->vk_frame_done) != VK_SUCCESS) {
        fprintf(stderr, "Failed to create semaphore vk_frame_done.\n");
        return false;
    }

    if (vkCreateSemaphore(ctx->dev, &sema_info, 0, &semaphores->vk_frame_ready) != VK_SUCCESS) {
        fprintf(stderr, "Failed to create semaphore vk_frame_ready.\n");
        return false;
    }
    sema_info.pNext = NULL;

    if (vkCreateSemaphore(ctx->dev, &sema_info, 0, &semaphores->vk_acquire_done) != VK_SUCCESS) {
        fprintf(stderr, "Failed to create semaphore vk_acquire_done.\n");
        return false;
    }

    return true;
}

void
vk_destroy_semaphores(struct vk_ctx *ctx,
              struct vk_semaphores *semaphores)
{
    if (semaphores->vk_frame_done)
        vkDestroySemaphore(ctx->dev, semaphores->vk_frame_done, 0);
    if (semaphores->vk_frame_ready)
        vkDestroySemaphore(ctx->dev, semaphores->vk_frame_ready, 0);
}

VkAccessFlags
access_flags_for_layout(VkImageLayout layout)
{
    switch (layout) {
        case VK_IMAGE_LAYOUT_PREINITIALIZED:
            return VK_ACCESS_HOST_WRITE_BIT;
        case VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL:
            return VK_ACCESS_TRANSFER_WRITE_BIT;
        case VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL:
            return VK_ACCESS_TRANSFER_READ_BIT;
        case VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL:
            return VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
        case VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL:
            return VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;
        case VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL:
            return VK_ACCESS_SHADER_READ_BIT;
        default:
            return 0;
    }
}

VkPipelineStageFlags
pipeline_stage_for_layout(VkImageLayout layout)
{
    switch (layout) {
        case VK_IMAGE_LAYOUT_UNDEFINED:
           return VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
        case VK_IMAGE_LAYOUT_PREINITIALIZED:
            return VK_PIPELINE_STAGE_HOST_BIT;
        case VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL:
        case VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL:
            return VK_PIPELINE_STAGE_TRANSFER_BIT;
        case VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL:
            return VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
        case VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL:
            return VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT;
        case VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL:
            return VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
        default:
            return VK_PIPELINE_STAGE_ALL_GRAPHICS_BIT;
    }
}

bool
vk_set_image_layout(struct vk_ctx *ctx,
                    struct vk_image_obj *img,
                    struct vk_semaphores *sem,
                    VkImageLayout old_layout,
                    VkImageLayout new_layout)
{
    VkCommandBufferAllocateInfo allocInfo = {};
    allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    allocInfo.commandPool = ctx->cmd_pool;
    allocInfo.commandBufferCount = 1;

    VkCommandBuffer commandBuffer;
    vkAllocateCommandBuffers(ctx->dev, &allocInfo, &commandBuffer);

    VkCommandBufferBeginInfo beginInfo = {};
    beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

    vkBeginCommandBuffer(commandBuffer, &beginInfo);

    VkImageMemoryBarrier barrier = {};
    barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
    barrier.oldLayout = old_layout;
    barrier.newLayout = new_layout;

    barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;

    barrier.image = img->img;
    barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    barrier.subresourceRange.baseMipLevel = 0;
    barrier.subresourceRange.levelCount = 1;
    barrier.subresourceRange.baseArrayLayer = 0;
    barrier.subresourceRange.layerCount = 1;

    VkPipelineStageFlags sourceStage;
    VkPipelineStageFlags destinationStage;

    barrier.srcAccessMask = access_flags_for_layout(old_layout);
    barrier.dstAccessMask = access_flags_for_layout(new_layout);
    sourceStage = pipeline_stage_for_layout(old_layout);
    destinationStage = pipeline_stage_for_layout(new_layout);

    vkCmdPipelineBarrier(
        commandBuffer,
        sourceStage, destinationStage,
        0,
        0, nullptr,
        0, nullptr,
        1, &barrier
    );

    vkEndCommandBuffer(commandBuffer);

    VkSubmitInfo submitInfo = {};
    submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    submitInfo.commandBufferCount = 1;
    submitInfo.pCommandBuffers = &commandBuffer;

    if (sem) {
        VkPipelineStageFlags stage_flags = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;

        submitInfo.pWaitDstStageMask = &stage_flags;
        submitInfo.waitSemaphoreCount = 1;
        submitInfo.pWaitSemaphores = &sem->vk_acquire_done;
        submitInfo.signalSemaphoreCount = 1;
        submitInfo.pSignalSemaphores = &sem->vk_frame_ready;
    }

    vkQueueSubmit(ctx->queue, 1, &submitInfo, VK_NULL_HANDLE);
    vkQueueWaitIdle(ctx->queue);

    vkFreeCommandBuffers(ctx->dev, ctx->cmd_pool, 1, &commandBuffer);

    return true;
}

static void
vk_get_device_surface_caps(struct vk_ctx* ctx)
{
    vk_swapchian_details* details = &ctx->swapchain_details;

    vkGetPhysicalDeviceSurfaceCapabilitiesKHR(ctx->pdev, ctx->surface, &details->caps);

    unsigned formatCount;
    vkGetPhysicalDeviceSurfaceFormatsKHR(ctx->pdev, ctx->surface, &formatCount, nullptr);

    if (formatCount != 0) {
        details->formats.resize(formatCount);
        vkGetPhysicalDeviceSurfaceFormatsKHR(ctx->pdev, ctx->surface, &formatCount, details->formats.data());
    }

    unsigned presentModeCount;
    vkGetPhysicalDeviceSurfacePresentModesKHR(ctx->pdev, ctx->surface, &presentModeCount, nullptr);

    if (presentModeCount != 0) {
        details->present_mode.resize(presentModeCount);
        vkGetPhysicalDeviceSurfacePresentModesKHR(ctx->pdev, ctx->surface, &presentModeCount, details->present_mode.data());
    }

    VkBool32 present = false;
    vkGetPhysicalDeviceSurfaceSupportKHR(ctx->pdev, 0, ctx->surface, &present);
}


bool
vk_create_swapchain(struct vk_ctx *ctx, unsigned w, unsigned h)
{
    unsigned img_count;
    vk_get_device_surface_caps(ctx);

    // TODO: create sth like swapchainCaps
    VkSurfaceFormatKHR surfaceFormat = { .format = VK_FORMAT_B8G8R8A8_UNORM,
                                         .colorSpace = VK_COLOR_SPACE_SRGB_NONLINEAR_KHR };
    VkExtent2D extent = { .width=w, .height=h };

    VkSwapchainCreateInfoKHR createInfo = {};
    createInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
    createInfo.surface = ctx->surface;

    createInfo.minImageCount = 1;
    createInfo.imageFormat = surfaceFormat.format;
    createInfo.imageColorSpace = surfaceFormat.colorSpace;
    createInfo.imageExtent = extent;
    createInfo.imageArrayLayers = 1;
    createInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;

    createInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
    createInfo.queueFamilyIndexCount = 0; // Optional
    createInfo.pQueueFamilyIndices = nullptr; // Optional

    createInfo.preTransform = VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR;
    createInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
    createInfo.presentMode = VK_PRESENT_MODE_FIFO_KHR;
    //createInfo.presentMode = VK_PRESENT_MODE_MAILBOX_KHR;
    createInfo.clipped = VK_TRUE;
    createInfo.oldSwapchain = VK_NULL_HANDLE;

    if (vkCreateSwapchainKHR(ctx->dev, &createInfo, NULL, &ctx->swapchain) != VK_SUCCESS) {
        fprintf(stderr, "failed to create swap chain!\n");
        return false;
    }

    vkGetSwapchainImagesKHR(ctx->dev, ctx->swapchain, &img_count, nullptr);
    printf("Swap chian image count: %d\n", img_count);

    ctx->swapchian_imgs.resize(img_count);
    vkGetSwapchainImagesKHR(ctx->dev, ctx->swapchain, &img_count, ctx->swapchian_imgs.data());

    ctx->surface_format = surfaceFormat;
    ctx->surface_extent = extent;

    ctx->swapchian_imgviews.resize(img_count);
    for (unsigned i = 0; i < img_count; i++) {
        vk_create_image_view(ctx, ctx->swapchian_imgs[i], &ctx->swapchian_imgviews[i]);
    }

    ctx->swapchain_setup = true;
    return true;
}

bool
vk_create_descriptors_layout(struct vk_ctx *ctx, struct vk_descriptor_layout *desc)
{
    std::array<VkDescriptorPoolSize, 2> poolSizes = {};
    poolSizes[0].type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    poolSizes[0].descriptorCount = 1;
    poolSizes[1].type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    poolSizes[1].descriptorCount = 1;

    VkDescriptorPoolCreateInfo desc_pool_info = {};
    desc_pool_info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
    desc_pool_info.poolSizeCount = static_cast<uint32_t>(poolSizes.size());
    desc_pool_info.pPoolSizes = poolSizes.data();
    desc_pool_info.maxSets = 1;

    if (vkCreateDescriptorPool(ctx->dev, &desc_pool_info, nullptr, &desc->desc_pool) != VK_SUCCESS) {
        fprintf(stderr, "failed to create descriptor pool!\n");
        return false;
    }

    VkDescriptorSetLayoutBinding uboLayoutBinding = {};
    uboLayoutBinding.binding = 0;
    uboLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    uboLayoutBinding.descriptorCount = 1;

    uboLayoutBinding.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;
    uboLayoutBinding.pImmutableSamplers = nullptr; // Optional

    VkDescriptorSetLayoutBinding samplerLayoutBinding = {};
    samplerLayoutBinding.binding = 1;
    samplerLayoutBinding.descriptorCount = 1;
    samplerLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    samplerLayoutBinding.pImmutableSamplers = nullptr;
    samplerLayoutBinding.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;

    std::array<VkDescriptorSetLayoutBinding, 2> bindings = {uboLayoutBinding, samplerLayoutBinding};

    VkDescriptorSetLayoutCreateInfo layoutInfo = {};
    layoutInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
    layoutInfo.bindingCount = static_cast<uint32_t>(bindings.size());
    layoutInfo.pBindings = bindings.data();


    if (vkCreateDescriptorSetLayout(ctx->dev, &layoutInfo, nullptr, &desc->desc_set_layout) != VK_SUCCESS) {
        fprintf(stderr, "failed to create descriptor set layout!\n");
        return false;
    }

    return true;
}

bool
vk_create_descriptors_set(struct vk_ctx *ctx, struct vk_image_att *tex, struct vk_buf *ubo, struct vk_descriptor_layout *desc)
{
    VkDescriptorSetAllocateInfo allocInfo = {};
    allocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
    allocInfo.descriptorPool = desc->desc_pool;
    allocInfo.descriptorSetCount = 1;
    allocInfo.pSetLayouts = &desc->desc_set_layout;

    desc->desc_sets.resize(1);
    if (vkAllocateDescriptorSets(ctx->dev, &allocInfo, desc->desc_sets.data()) != VK_SUCCESS) {
        fprintf(stderr, "failed to allocate descriptor sets!\n");
        return false;
    }

    vk_create_sampler(ctx, &ctx->sampler);
    vk_create_image_view(ctx, tex->obj.img, &tex->view);

    VkDescriptorBufferInfo bufferInfo = {};
    bufferInfo.buffer = ubo->buf;
    bufferInfo.offset = 0;
    bufferInfo.range = ubo->size;

    VkDescriptorImageInfo imageInfo = {};
    imageInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
    //imageInfo.imageLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
    imageInfo.imageView = tex->view;
    imageInfo.sampler = ctx->sampler;

    std::array<VkWriteDescriptorSet, 2> descriptorWrites = {};

    descriptorWrites[0].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    descriptorWrites[0].dstSet = desc->desc_sets[0];
    descriptorWrites[0].dstBinding = 0;
    descriptorWrites[0].dstArrayElement = 0;
    descriptorWrites[0].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    descriptorWrites[0].descriptorCount = 1;
    descriptorWrites[0].pBufferInfo = &bufferInfo;

    descriptorWrites[1].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    descriptorWrites[1].dstSet = desc->desc_sets[0];
    descriptorWrites[1].dstBinding = 1;
    descriptorWrites[1].dstArrayElement = 0;
    descriptorWrites[1].descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    descriptorWrites[1].descriptorCount = 1;
    descriptorWrites[1].pImageInfo = &imageInfo;

    vkUpdateDescriptorSets(ctx->dev, 2, descriptorWrites.data(), 0, nullptr);

    return true;
}
