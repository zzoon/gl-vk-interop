/* Mostly taken from piglit.
 */

#ifndef INTEROPERABILITY_H
#define INTEROPERABILITY_H

#include <GL/gl.h>
#include "vk.h"

typedef void (* PFN_glCreateMemoryObjectsEXT) (GLsizei n,
                                               GLuint *memoryObjects);
typedef void (* PFN_glDeleteMemoryObjectsEXT) (GLsizei n,
                                               GLuint *memoryObjects);
typedef void (* PFN_glImportMemoryFdEXT) (GLuint memory, GLuint64 size, GLenum handleType, GLint fd);
typedef void (* PFN_glTexStorageMem1DEXT) (GLenum target, GLsizei levels, GLenum internalFormat, GLsizei width, GLuint memory, GLuint64 offset);
typedef void (* PFN_glTexStorageMem2DEXT) (GLenum target, GLsizei levels, GLenum internalFormat, GLsizei width, GLsizei height, GLuint memory, GLuint64 offset);
typedef void (* PFN_glTexStorageMem3DEXT) (GLenum target, GLsizei levels, GLenum internalFormat, GLsizei width, GLsizei height, GLsizei depth, GLuint memory, GLuint64 offset);

typedef bool (* PFN_glIsMemoryObjectEXT) (GLuint target);

typedef bool (* PFN_glGenSemaphoresEXT) (GLsizei n, GLuint *semaphores);
typedef bool (* PFN_glImportSemaphoreFdEXT) (GLuint semaphore, GLenum handleType, GLint fd);
typedef bool (* PFN_glIsSemaphoreEXT) (GLuint semaphore);
typedef bool (* PFN_glSignalSemaphoreEXT) (GLuint semaphore, GLuint numBufferBarriers, const GLuint* buffers, GLuint numTextureBarriers, const GLuint *textures, const GLenum *srcLayouts);
typedef bool (* PFN_glWaitSemaphoreEXT) (GLuint semaphore, GLuint numBufferBarriers, const GLuint* buffers, GLuint numTextureBarriers, const GLuint *textures, const GLenum *srcLayouts);

typedef bool (* PFN_glCreateBuffers) (GLsizei n, GLuint *buffers);
typedef bool (* PFN_glNamedBufferStorageMemEXT) (GLuint buffer, GLsizeiptr size, GLuint memory, GLuint64 offset);
typedef bool (* PFN_glBufferStorageMemEXT) (GLenum target, GLsizeiptr size, GLuint memory, GLuint64 offset);
typedef bool (* PFN_glIsMemoryObjectEXT) (GLuint target);

struct gl_ext_semaphores {
    GLuint gl_frame_ready;
    GLuint gl_frame_done;
};

enum fragment_type {
    FLOAT_FS = 0,
    INT_FS,
    UINT_FS,
};

struct format_mapping {
    char *name;
    GLenum glformat;
    VkFormat vkformat;
    enum fragment_type fs_type;

    uint32_t rbits;
    uint32_t gbits;
    uint32_t bbits;
    uint32_t abits;

    VkImageTiling tiling;
    VkImageUsageFlagBits usage;
};

GLuint
gl_get_target(const struct vk_image_props *props);

bool
gl_create_mem_obj_from_vk_mem(struct vk_ctx *ctx,
                  VkDeviceMemory *vk_mem_obj,
                  VkDeviceSize mem_size,
                  GLuint *gl_mem_obj);

bool
gl_gen_buf_from_mem_obj(GLuint mem_obj, uint32_t size, GLuint *buf_obj);

bool
gl_gen_tex_from_mem_obj(const struct vk_image_props *props,
            GLenum gl_format,
            GLuint mem_obj, uint32_t offset,
            GLuint *tex);

bool
gl_create_semaphores_from_vk(const struct vk_ctx *ctx,
                 const struct vk_semaphores *vk_smps,
                 struct gl_ext_semaphores *gl_smps);

GLenum
gl_get_layout_from_vk(const VkImageLayout vk_layout);


#endif /* INTEROPERABILITY_H */
