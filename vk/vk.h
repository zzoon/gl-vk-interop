/* Mostly taken from piglit.
 */

#ifndef VK_H
#define VK_H

#include <vulkan/vulkan.h>
#include <vector>

struct vk_descriptor_layout {
    VkDescriptorSetLayout desc_set_layout;
    VkDescriptorPool desc_pool;
    std::vector<VkDescriptorSet> desc_sets;
};

struct vk_swapchian_details {
    VkSurfaceCapabilitiesKHR caps;
    std::vector<VkSurfaceFormatKHR> formats;
    std::vector<VkPresentModeKHR> present_mode;
};

struct vk_ctx
{
    VkInstance inst;
    VkPhysicalDevice pdev;
    VkDevice dev;
    VkPipelineCache cache;
    VkCommandPool cmd_pool;
    VkCommandBuffer cmd_buf;
    VkQueue queue;

    VkSwapchainKHR swapchain;
    std::vector<VkImage> swapchian_imgs;
    std::vector<VkImageView> swapchian_imgviews;
    struct vk_swapchian_details swapchain_details;
    VkSurfaceKHR surface;
    VkSurfaceFormatKHR surface_format;
    VkExtent2D surface_extent;
    struct vk_descriptor_layout desc_layout;
    VkSampler sampler;
    bool swapchain_setup;

    uint8_t deviceUUID[VK_UUID_SIZE];
    uint8_t driverUUID[VK_UUID_SIZE];
};

struct vk_image_props
{
    uint32_t w;
    uint32_t h;
    uint32_t depth;

    uint32_t num_samples;
    uint32_t num_levels;
    uint32_t num_layers;

    VkFormat format;
    VkImageUsageFlags usage;
    VkImageTiling tiling;

    VkImageLayout in_layout;
    VkImageLayout end_layout;
};

struct vk_image_obj
{
    VkImage img;
    VkDeviceMemory mem;
    VkDeviceSize mem_sz;
};

struct vk_image_att {
    struct vk_image_obj obj;
    struct vk_image_props props;
    VkImageView view;
};

struct vk_renderer
{
    VkPipeline pipeline;
    VkPipelineLayout pipeline_layout;
    VkRenderPass renderpass;
    VkShaderModule vs;
    VkShaderModule fs;
    VkFramebuffer fb;
};

struct vk_compute
{
    VkPipeline pipeline;
    VkPipelineLayout pipeline_layout;
    VkShaderModule cs;
};

struct vk_buf
{
    VkBuffer buf;
    VkDeviceMemory mem;
    VkDeviceSize size;
};

struct vk_cmdbuf
{
    VkCommandBuffer cmd;
};

struct vk_semaphores
{
    VkSemaphore vk_frame_ready;
    VkSemaphore vk_frame_done;
    VkSemaphore vk_acquire_done;
};

struct vk_dims
{
    float w;
    float h;
};

bool
vk_init_ctx(struct vk_ctx *ctx, const char **extensions, int extension_size);

bool
vk_init_ctx_for_rendering(struct vk_ctx *ctx, const char **extensions, int extension_size);

void
vk_cleanup_ctx(struct vk_ctx *ctx);

bool
vk_check_gl_compatibility(struct vk_ctx *ctx);

bool
vk_create_sampler(struct vk_ctx *ctx, VkSampler *sampler);

bool
vk_create_ext_image(struct vk_ctx *ctx,
            struct vk_image_props *props,
            struct vk_image_obj *img_obj);

void vk_destroy_ext_image(struct vk_ctx *ctx, struct vk_image_obj *img_obj);

bool
vk_fill_ext_image_props(struct vk_ctx *ctx,
            uint32_t w, uint32_t h,
            uint32_t depth,
            uint32_t num_samples,
            uint32_t num_levels,
            uint32_t num_layers,
            VkFormat format,
            VkImageTiling tiling,
            VkImageUsageFlags usage,
            VkImageLayout in_layout,
            VkImageLayout end_layout,
            struct vk_image_props *props);

bool
vk_create_image_view(struct vk_ctx *ctx, struct vk_image_att *img);

bool
vk_set_image_layout(struct vk_ctx *ctx,
                    struct vk_image_obj *img,
                    struct vk_semaphores *sem,
                    VkImageLayout old_layout,
                    VkImageLayout new_layout);

bool
vk_create_descriptors_layout(struct vk_ctx *ctx, struct vk_descriptor_layout *desc);

bool
vk_create_descriptors_set(struct vk_ctx *ctx, struct vk_image_att *tex, struct vk_buf *ubo, struct vk_descriptor_layout *desc);

bool
vk_create_swapchain(struct vk_ctx *ctx, unsigned w, unsigned h);

bool
vk_create_renderer(struct vk_ctx *ctx,
           const char *vs_path,
           const char *fs_path,
           struct vk_image_att *color_att,
           struct vk_image_att *depth_att,
           struct vk_renderer *renderer);

void
vk_destroy_renderer(struct vk_ctx *ctx,
            struct vk_renderer *pipeline);

bool
vk_create_buffer(struct vk_ctx *ctx,
         uint32_t sz,
         VkBufferUsageFlags usage,
         struct vk_buf *bo);

bool
vk_update_buffer_data(struct vk_ctx *ctx,
              void *data,
              uint32_t data_sz,
              struct vk_buf *bo);

void
vk_destroy_buffer(struct vk_ctx *ctx,
          struct vk_buf *bo);

void
vk_acquire(struct vk_ctx *ctx, vk_semaphores *sem, unsigned* img_idx);

void
vk_submit(struct vk_ctx *ctx, vk_semaphores *sem);

void
vk_present(struct vk_ctx *ctx, vk_semaphores *sem, unsigned img_idx);

void
vk_build_draw_cmd(struct vk_ctx *ctx,
    struct vk_buf *vbo,
    struct vk_buf *ibo,
    struct vk_dims *dims,
    struct vk_renderer *renderer,
    float *vk_fb_color,
    uint32_t vk_fb_color_count,
    uint32_t w, uint32_t h);

bool
vk_create_semaphores(struct vk_ctx *ctx,
             struct vk_semaphores *semaphores);

void
vk_destroy_semaphores(struct vk_ctx *ctx,
              struct vk_semaphores *semaphores);

#endif /* VK_H */
