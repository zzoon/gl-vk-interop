/* Mostly taken from piglit.
 */

#include <stdio.h>
#include <assert.h>
#include <GLFW/glfw3.h>
#include <GL/glext.h>
#include "interop.h"

GLuint
gl_get_target(const struct vk_image_props *props)
{
    if (props->h == 1)
        return GL_TEXTURE_1D;

    if (props->depth > 1)
        return GL_TEXTURE_3D;

    return GL_TEXTURE_2D;
}

bool
gl_create_mem_obj_from_vk_mem(struct vk_ctx *ctx,
                  VkDeviceMemory *vk_mem_obj,
                  VkDeviceSize mem_size,
                  GLuint *gl_mem_obj)
{
    VkMemoryGetFdInfoKHR fd_info = {};
    int fd;

    PFN_glCreateMemoryObjectsEXT glCreateMemoryObjectsEXT =
       (PFN_glCreateMemoryObjectsEXT) glfwGetProcAddress ("glCreateMemoryObjectsEXT");
    assert (glCreateMemoryObjectsEXT != NULL);

    PFN_glImportMemoryFdEXT glImportMemoryFdEXT =
       (PFN_glImportMemoryFdEXT) glfwGetProcAddress ("glImportMemoryFdEXT");
    assert (glImportMemoryFdEXT != NULL);

    PFN_glIsMemoryObjectEXT glIsMemoryObjectEXT =
       (PFN_glIsMemoryObjectEXT) glfwGetProcAddress ("glIsMemoryObjectEXT");
    assert (glIsMemoryObjectEXT != NULL);

    PFN_vkGetMemoryFdKHR _vkGetMemoryFdKHR =
        (PFN_vkGetMemoryFdKHR)vkGetDeviceProcAddr(ctx->dev,
                "vkGetMemoryFdKHR");

    if (!_vkGetMemoryFdKHR) {
        fprintf(stderr, "vkGetMemoryFdKHR not found\n");
        return false;
    }

    fd_info.sType = VK_STRUCTURE_TYPE_MEMORY_GET_FD_INFO_KHR;
    fd_info.memory = *vk_mem_obj;
    fd_info.handleType = VK_EXTERNAL_MEMORY_HANDLE_TYPE_OPAQUE_FD_BIT;

    if (_vkGetMemoryFdKHR(ctx->dev, &fd_info, &fd) != VK_SUCCESS) {
        fprintf(stderr, "Failed to get the Vulkan memory FD");
        return false;
    }

    glCreateMemoryObjectsEXT(1, gl_mem_obj);
    glImportMemoryFdEXT(*gl_mem_obj, mem_size, GL_HANDLE_TYPE_OPAQUE_FD_EXT, fd);

    if (!glIsMemoryObjectEXT(*gl_mem_obj))
        return false;

    return glGetError() == GL_NO_ERROR;
}

bool
gl_gen_buf_from_mem_obj(GLuint mem_obj, uint32_t size, GLuint *buf_obj)
{
    PFN_glCreateBuffers glCreateBuffers =
       (PFN_glCreateBuffers) glfwGetProcAddress ("glCreateBuffers");
    assert (glCreateBuffers != NULL);

    PFN_glNamedBufferStorageMemEXT glNamedBufferStorageMemEXT =
       (PFN_glNamedBufferStorageMemEXT) glfwGetProcAddress ("glNamedBufferStorageMemEXT");
    assert (glNamedBufferStorageMemEXT != NULL);

    glCreateBuffers(1, buf_obj);
    assert (glGetError () == GL_NO_ERROR);

    glNamedBufferStorageMemEXT(*buf_obj, size, mem_obj, 0);
    assert (glGetError () == GL_NO_ERROR);

    return true;
}

bool
gl_gen_tex_from_mem_obj(const struct vk_image_props *props,
            GLenum tex_storage_format,
            GLuint mem_obj, uint32_t offset,
            GLuint *tex)
{
    GLuint target = gl_get_target(props);

    PFN_glTexStorageMem1DEXT glTexStorageMem1DEXT =
       (PFN_glTexStorageMem1DEXT) glfwGetProcAddress ("glTexStorageMem1DEXT");
    assert (glTexStorageMem1DEXT != NULL);

    PFN_glTexStorageMem2DEXT glTexStorageMem2DEXT =
       (PFN_glTexStorageMem2DEXT) glfwGetProcAddress ("glTexStorageMem2DEXT");
    assert (glTexStorageMem2DEXT != NULL);

    PFN_glTexStorageMem3DEXT glTexStorageMem3DEXT =
       (PFN_glTexStorageMem3DEXT) glfwGetProcAddress ("glTexStorageMem3DEXT");
    assert (glTexStorageMem3DEXT != NULL);

    GLint tiling = props->tiling == VK_IMAGE_TILING_LINEAR ? GL_LINEAR_TILING_EXT :
                    GL_OPTIMAL_TILING_EXT;

    glGenTextures(1, tex);
    glBindTexture(target, *tex);
    glTexParameteri(target, GL_TEXTURE_TILING_EXT, tiling);

    switch (target) {
    case GL_TEXTURE_1D:
        assert(props->depth == 1);
        glTexStorageMem1DEXT(target, props->num_levels,
                     tex_storage_format,
                     props->w,
                     mem_obj, offset);
        break;
    case GL_TEXTURE_2D:
        assert(props->depth == 1);
        glTexStorageMem2DEXT(target, props->num_levels,
                     tex_storage_format,
                     props->w, props->h,
                     mem_obj, offset);
        break;
    case GL_TEXTURE_3D:
        glTexStorageMem3DEXT(target, props->num_levels,
                     tex_storage_format,
                     props->w, props->h, props->depth,
                     mem_obj, offset);
        break;
    default:
        fprintf(stderr, "Invalid GL texture target\n");
        return false;
    }

    glTexParameteri(target, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(target, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    return glGetError() == GL_NO_ERROR;
}

bool
gl_create_semaphores_from_vk(const struct vk_ctx *ctx,
                 const struct vk_semaphores *vk_smps,
                 struct gl_ext_semaphores *gl_smps)
{
    VkSemaphoreGetFdInfoKHR sem_fd_info = {};
    int fd_vk_ready;
    int fd_vk_done;

    PFN_glGenSemaphoresEXT glGenSemaphoresEXT =
       (PFN_glGenSemaphoresEXT) glfwGetProcAddress ("glGenSemaphoresEXT");
    assert (glGenSemaphoresEXT != NULL);

    PFN_glImportSemaphoreFdEXT glImportSemaphoreFdEXT =
       (PFN_glImportSemaphoreFdEXT) glfwGetProcAddress ("glImportSemaphoreFdEXT");
    assert (glImportSemaphoreFdEXT != NULL);

    PFN_glIsSemaphoreEXT glIsSemaphoreEXT =
       (PFN_glIsSemaphoreEXT) glfwGetProcAddress ("glIsSemaphoreEXT");
    assert (glIsSemaphoreEXT != NULL);

    PFN_vkGetSemaphoreFdKHR _vkGetSemaphoreFdKHR;

    glGenSemaphoresEXT(1, &gl_smps->gl_frame_done);
    glGenSemaphoresEXT(1, &gl_smps->gl_frame_ready);

    _vkGetSemaphoreFdKHR =
        (PFN_vkGetSemaphoreFdKHR)vkGetDeviceProcAddr(ctx->dev,
                                 "vkGetSemaphoreFdKHR");
    if (!_vkGetSemaphoreFdKHR) {
        fprintf(stderr, "vkGetSemaphoreFdKHR not found\n");
        return false;
    }

    sem_fd_info.sType = VK_STRUCTURE_TYPE_SEMAPHORE_GET_FD_INFO_KHR;
    sem_fd_info.semaphore = vk_smps->vk_frame_done;
    sem_fd_info.handleType = VK_EXTERNAL_SEMAPHORE_HANDLE_TYPE_OPAQUE_FD_BIT_KHR;

    if (_vkGetSemaphoreFdKHR(ctx->dev, &sem_fd_info, &fd_vk_done) != VK_SUCCESS) {
        fprintf(stderr, "Failed to get the Vulkan memory FD");
        return false;
    }

    sem_fd_info.semaphore = vk_smps->vk_frame_ready;
    if (_vkGetSemaphoreFdKHR(ctx->dev, &sem_fd_info, &fd_vk_ready) != VK_SUCCESS) {
        fprintf(stderr, "Failed to get the Vulkan memory FD");
        return false;
    }

    glImportSemaphoreFdEXT(gl_smps->gl_frame_ready,
                   GL_HANDLE_TYPE_OPAQUE_FD_EXT,
                   fd_vk_done);

    glImportSemaphoreFdEXT(gl_smps->gl_frame_done,
                   GL_HANDLE_TYPE_OPAQUE_FD_EXT,
                   fd_vk_ready);

    if (!glIsSemaphoreEXT(gl_smps->gl_frame_done))
        return false;

    if (!glIsSemaphoreEXT(gl_smps->gl_frame_ready))
        return false;

    return glGetError() == GL_NO_ERROR;
}

GLenum
gl_get_layout_from_vk(const VkImageLayout vk_layout)
{
    switch (vk_layout) {
    case VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL:
        return GL_LAYOUT_COLOR_ATTACHMENT_EXT;
    case VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL:
        return GL_LAYOUT_DEPTH_STENCIL_ATTACHMENT_EXT;
    case VK_IMAGE_LAYOUT_DEPTH_STENCIL_READ_ONLY_OPTIMAL:
        return GL_LAYOUT_DEPTH_STENCIL_READ_ONLY_EXT;
    case VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL:
        return GL_LAYOUT_SHADER_READ_ONLY_EXT;
    case VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL:
        return GL_LAYOUT_TRANSFER_SRC_EXT;
    case VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL:
        return GL_LAYOUT_TRANSFER_DST_EXT;
    case VK_IMAGE_LAYOUT_DEPTH_READ_ONLY_STENCIL_ATTACHMENT_OPTIMAL_KHR:
        return GL_LAYOUT_DEPTH_READ_ONLY_STENCIL_ATTACHMENT_EXT;
    case VK_IMAGE_LAYOUT_DEPTH_ATTACHMENT_STENCIL_READ_ONLY_OPTIMAL_KHR:
        return GL_LAYOUT_DEPTH_ATTACHMENT_STENCIL_READ_ONLY_EXT;
    case VK_IMAGE_LAYOUT_UNDEFINED:
    default:
        return GL_NONE;
    };
}

