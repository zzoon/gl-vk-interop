#ifndef _VERTEX_VK_H_
#define _VERTEX_VK_H_

#include <glm/glm.hpp>
struct Vertex {
    glm::vec2 pos;
    glm::vec3 color;
    glm::vec2 uv;
};

#endif
