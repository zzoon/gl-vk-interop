
CXXFLAGS += -g
LDFLAGS =

#PKG_CONFIG_MODULES = vulkan

#ifdef PKG_CONFIG_MODULES
#	CXXFLAGS  += $(shell pkg-config --cflags $(PKG_CONFIG_MODULES))
#	LDFLAGS += $(shell pkg-config --libs   $(PKG_CONFIG_MODULES))
#endif

EXE = gl-vk-interop
EXE2 = vk-gl-interop

all: $(TARGETS)

SOURCES +=  gl3w/GL/gl3w.c
SOURCES +=  imgui/imgui_impl_glfw.cpp \
			imgui/imgui_impl_opengl3.cpp \
			imgui/imgui.cpp \
			imgui/imgui_demo.cpp \
			imgui/imgui_draw.cpp \
			imgui/imgui_widgets.cpp
SOURCES +=  vk/interop.cpp

SOURCES_1 = $(SOURCES) main.cpp
SOURCES_2 = $(SOURCES) 2nd_exam.cpp

OBJS_1 = $(addsuffix .o, $(basename $(notdir $(SOURCES_1))))
OBJS_2 = $(addsuffix .o, $(basename $(notdir $(SOURCES_2))))

CXXFLAGS += -I. -I./imgui -I./gl3w -DIMGUI_IMPL_OPENGL_LOADER_GL3W
CXXFLAGS += -g -Wall -Wformat

CXXFLAGS_EXTRA = $(CXXFLAGS) -DVK_PRESENT

LIBS += -lGL `pkg-config --static --libs glfw3 ` -lpthread
LIBS += `pkg-config --libs vulkan `
CXXFLAGS += `pkg-config --cflags glfw3 vulkan`
CFLAGS = $(CXXFLAGS)


##---------------------------------------------------------------------
## BUILD RULES
##---------------------------------------------------------------------

.PHONY: vk.o

%.o:%.cpp
	$(CXX) $(CXXFLAGS) -c -o $@ $<

%.o:./vk/%.cpp
	$(CXX) $(CXXFLAGS) -c -o $@ $<

%.o:./imgui/%.cpp
	$(CXX) $(CXXFLAGS) -c -o $@ $<

%.o:./gl3w/GL/%.c
	$(CC) $(CFLAGS) -c -o $@ $<

vk_1.o:./vk/vk.cpp
	$(CXX) $(CXXFLAGS) -c -o $@ $<

vk_2.o:./vk/vk.cpp
	$(CXX) $(CXXFLAGS_EXTRA) -c -o $@ $<

all: $(EXE) $(EXE2)
	@echo Build complete for Linux

$(EXE): $(OBJS_1) vk_1.o
	$(CXX) $^ $(CXXFLAGS) $(LIBS) -o $@

$(EXE2): $(OBJS_2) vk_2.o
	$(CXX) $^ $(CXXFLAGS) $(LIBS) -o $@

clean:
	rm -f $(EXE) $(EXE2) $(OBJS_1) $(OBJS_2) vk_1.o vk_2.o

