#include "imgui.h"
#include "imgui_impl_glfw.h"
#include "imgui_impl_vulkan.h"
#include <stdio.h>
#include <time.h>
#include <sys/time.h>
#include <fcntl.h>
#include <pthread.h>
#include <unistd.h>
#include <gbm.h>
#include <iostream>
#include <vector>
#include <chrono>

// For vulkan
#include <vulkan/vulkan.h>
#include <GL/gl3w.h>            // Initialize with gl3wInit()
#include <GLFW/glfw3.h>
#include <GL/glext.h>

#include "vk/vk.h"
#include "vk/interop.h"
#include "vk/vertex_vk.h"

#include <glm/gtc/matrix_transform.hpp>

#define TEX_WIDTH 1280
#define TEX_HEIGHT 960

GLchar const* vertexShaderSource = {R"(
    #version 430
    #extension GL_ARB_separate_shader_objects : enable

    const vec2 vdata[] = vec2[] (
    		vec2(1.0, 1.0),
    		vec2(1.0, 0.0),
    		vec2(0.0, 1.0),
    		vec2(0.0, 0.0));

    void main()
    {
    	gl_Position = vec4(vdata[gl_VertexID] * 2.0 - 1.0, 0.0, 1.0);
    }
)"};

GLchar const* fragmentShaderSource = {R"(
    #version 430

    layout(location = 0) uniform vec2 dims;
    layout(location = 0) out vec4 f_color;

    const vec4 colors[] = vec4[] (
    	vec4(1.0, 0.0, 0.0, 1.0),
    	vec4(0.0, 1.0, 0.0, 1.0),
    	vec4(0.0, 0.0, 1.0, 1.0),
    	vec4(1.0, 1.0, 0.0, 1.0),
    	vec4(1.0, 0.0, 1.0, 1.0),
    	vec4(0.0, 1.0, 1.0, 1.0));

    void main()
    {
    	int band = int(gl_FragCoord.x * 6.0 / dims.x);
    	f_color = colors[band];
    }
)"};

static float  vk_fb_color[4] = { 0.45, 0.55, 0.60, 1.0 };
static struct vk_ctx vk_core;
static struct vk_semaphores vk_sem;
static struct vk_image_att vk_img;
static struct vk_renderer vk_render;
static struct vk_buf vk_ubo;
static struct vk_buf vk_vtx;
static struct vk_buf vk_idx;
static struct vk_buf vtx_buf;

struct UniformBufferObject {
    glm::mat4 model;
    glm::mat4 view;
    glm::mat4 proj;
};

struct gl_data {
    GLuint gl_tex;
    GLuint gl_vtx;
    GLuint gl_vtx_mem;
    GLuint gl_vao;
    GLuint gl_fbo;
    struct gl_ext_semaphores gl_sem;
};

static struct gl_data gl_data = {0, };

static std::vector<Vertex> g_vertices = {
    {{-0.5f, -0.5f}, {1.0f, 0.0f, 0.0f}, {1.0f, 0.0f}},
    {{0.5f, -0.5f}, {0.0f, 1.0f, 0.0f}, {0.0f, 0.0f}},
    {{0.5f, 0.5f}, {0.0f, 0.0f, 1.0f}, {0.0f, 1.0f}},
    {{-0.5f, 0.5f}, {1.0f, 1.0f, 1.0f}, {1.0f, 1.0f}}
};

const std::vector<uint16_t> indices = {
    0, 1, 2, 2, 3, 0
};


struct Vertex_gl {
    glm::vec3 pos;
};

static std::vector<Vertex_gl> g_vertex_data = {{{-1.0f, 1.0f, 0.0f}},
                                             {{ 1.0f,  1.0f, 0.0f}},
                                             {{-1.0f, -1.0f, 0.0f}},
                                             {{ 1.0f, -1.0f, 0.0f}}};


static
void vk_init(GLFWwindow* window)
{
    VkResult ret;

    unsigned glfwExtensionCount = 0;
    const char** glfwExtensions;
    glfwExtensions = glfwGetRequiredInstanceExtensions(&glfwExtensionCount);

    if (!vk_init_ctx_for_rendering(&vk_core, glfwExtensions, glfwExtensionCount))
        fprintf(stderr, "Failed to initialize vulkan\n");

    if ((ret = glfwCreateWindowSurface(vk_core.inst, window, nullptr, &vk_core.surface)) != VK_SUCCESS) {
        fprintf(stderr, "Failed to create window surface: %d \n", ret);
        return;
    }

    vk_create_buffer(&vk_core,
                     sizeof(g_vertex_data[0]) * g_vertex_data.size(),
                     VK_BUFFER_USAGE_VERTEX_BUFFER_BIT, &vtx_buf);
    printf("Created a buffer in VK: size(%lu)\n", vtx_buf.size);


    vk_create_swapchain(&vk_core, 1280, 960);

    vk_create_semaphores(&vk_core, &vk_sem);

    vk_create_buffer(&vk_core,
                     sizeof(g_vertices[0]) * g_vertices.size(),
                     VK_BUFFER_USAGE_VERTEX_BUFFER_BIT, &vk_vtx);

    vk_create_buffer(&vk_core, sizeof(UniformBufferObject),
                     VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, &vk_ubo);

    vk_create_buffer(&vk_core, sizeof(indices[0]) * indices.size(),
                     VK_BUFFER_USAGE_INDEX_BUFFER_BIT, &vk_idx);

    void *data;
    vkMapMemory(vk_core.dev, vk_idx.mem, 0, vk_idx.size, 0, &data);
    memcpy(data, indices.data(), indices.size() * sizeof(indices[0]));
    vkUnmapMemory(vk_core.dev, vk_idx.mem);

    vkMapMemory(vk_core.dev, vk_vtx.mem, 0, vk_vtx.size, 0, &data);
    memcpy(data, g_vertices.data(), g_vertices.size() * sizeof(Vertex));
    vkUnmapMemory(vk_core.dev, vk_vtx.mem);

    uint32_t num_samples = 1;
    uint32_t num_levels = 1;
    uint32_t num_layers = 1;
    VkFormat color_format = VK_FORMAT_B8G8R8A8_UNORM;
    VkImageUsageFlags color_usage = VK_IMAGE_USAGE_SAMPLED_BIT |
                                    VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
    VkImageTiling color_tiling = VK_IMAGE_TILING_LINEAR;
    VkImageLayout color_in_layout = VK_IMAGE_LAYOUT_UNDEFINED;
    VkImageLayout color_end_layout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

    vk_fill_ext_image_props(&vk_core, TEX_WIDTH, TEX_HEIGHT, 1,
                            num_samples, num_levels, num_layers,
                            color_format, color_tiling, color_usage,
                            color_in_layout, color_end_layout, &vk_img.props);

    vk_create_ext_image(&vk_core, &vk_img.props, &vk_img.obj);

    vk_create_descriptors_layout(&vk_core, &vk_core.desc_layout);

    /* create Vulkan renderer */
    if (!vk_create_renderer(&vk_core,
                            "shaders/tex_shader.vert.spv",
                            "shaders/tex_shader.frag.spv",
                            &vk_img, NULL, &vk_render)) {
        fprintf(stderr, "Failed to create Vulkan renderer.\n");
        return;
    }

    vk_create_descriptors_set(&vk_core, &vk_img, &vk_ubo, &vk_core.desc_layout);

}

static void
opengl_init()
{
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);

    // Create window with graphics context
    GLFWwindow* window = glfwCreateWindow(10, 10, "GL-VK Interop example example", NULL, NULL);
    if (window == NULL)
        return;
    glfwMakeContextCurrent(window);

    bool err = gl3wInit() != 0;
    if (err)
    {
        fprintf(stderr, "Failed to initialize OpenGL loader!\n");
        return;
    }
}

static
void opengl_setup()
{
    // build and compile our shader program
    // ------------------------------------
    // vertex shader
    int vertexShader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertexShader, 1, &vertexShaderSource, NULL);
    glCompileShader(vertexShader);
    // check for shader compile errors
    int success;
    char infoLog[512];
    glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &success);
    if (!success)
    {
        glGetShaderInfoLog(vertexShader, 512, NULL, infoLog);
        std::cout << "ERROR::SHADER::VERTEX::COMPILATION_FAILED\n" << infoLog << std::endl;
    }
    // fragment shader
    int fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragmentShader, 1, &fragmentShaderSource, NULL);
    glCompileShader(fragmentShader);
    // check for shader compile errors
    glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &success);
    if (!success)
    {
        glGetShaderInfoLog(fragmentShader, 512, NULL, infoLog);
        std::cout << "ERROR::SHADER::FRAGMENT::COMPILATION_FAILED\n" << infoLog << std::endl;
    }
    // link shaders
    int shaderProgram = glCreateProgram();
    glAttachShader(shaderProgram, vertexShader);
    glAttachShader(shaderProgram, fragmentShader);

    glLinkProgram(shaderProgram);
    // check for linking errors
    glGetProgramiv(shaderProgram, GL_LINK_STATUS, &success);
    if (!success) {
        glGetProgramInfoLog(shaderProgram, 512, NULL, infoLog);
        std::cout << "ERROR::SHADER::PROGRAM::LINKING_FAILED\n" << infoLog << std::endl;
    }
    glDeleteShader(vertexShader);
    glDeleteShader(fragmentShader);

    // Get texture from vk
    GLuint gl_mem_obj;
    gl_create_mem_obj_from_vk_mem(&vk_core, &vk_img.obj.mem, vk_img.obj.mem_sz, &gl_mem_obj);
    gl_gen_tex_from_mem_obj(&vk_img.props, GL_RGBA8, gl_mem_obj, 0, &gl_data.gl_tex);

    glGenFramebuffers(1, &gl_data.gl_fbo);
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, gl_data.gl_fbo);
    glFramebufferTexture(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, gl_data.gl_tex, 0);
    glGenVertexArrays(1, &gl_data.gl_vao);
    glBindVertexArray(gl_data.gl_vao);
    glUseProgram(shaderProgram);
    glProgramUniform2f(shaderProgram, 0, (float)TEX_WIDTH, (float)TEX_HEIGHT);
    glViewport(0, 0, TEX_WIDTH, TEX_HEIGHT);
}

static void
opengl_draw()
{
    vk_set_image_layout(&vk_core, &vk_img.obj, &vk_sem, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL);

    PFN_glSignalSemaphoreEXT glSignalSemaphoreEXT =
       (PFN_glSignalSemaphoreEXT) glfwGetProcAddress ("glSignalSemaphoreEXT");
    assert (glSignalSemaphoreEXT != NULL);

    PFN_glSignalSemaphoreEXT glWaitSemaphoreEXT =
       (PFN_glWaitSemaphoreEXT) glfwGetProcAddress ("glWaitSemaphoreEXT");
    assert (glWaitSemaphoreEXT != NULL);

    // Wait (on the GPU side) for the Vulkan semaphore to be signaled
    GLenum srcLayout = GL_LAYOUT_COLOR_ATTACHMENT_EXT;
    glWaitSemaphoreEXT(gl_data.gl_sem.gl_frame_ready, 0, nullptr, 1, &gl_data.gl_tex, &srcLayout);

    // Draw to the framebuffer
    glBindVertexArray(gl_data.gl_vao);
    glBindTexture(GL_TEXTURE_2D, gl_data.gl_tex);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

    GLenum dstLayout = GL_LAYOUT_SHADER_READ_ONLY_EXT;
    //GLenum dstLayout = GL_LAYOUT_COLOR_ATTACHMENT_EXT;
    glSignalSemaphoreEXT(gl_data.gl_sem.gl_frame_done, 0, nullptr, 1, &gl_data.gl_tex, &dstLayout);
}

static void
animate()
{
    static auto startTime = std::chrono::high_resolution_clock::now();

    auto currentTime = std::chrono::high_resolution_clock::now();
    float time = std::chrono::duration<float, std::chrono::seconds::period>(currentTime - startTime).count();

    UniformBufferObject ubo = {};
    ubo.model = glm::rotate(glm::mat4(1.0f), time * glm::radians(90.0f), glm::vec3(0.0f, 0.0f, 1.0f));
    ubo.view = glm::lookAt(glm::vec3(2.0f, 2.0f, 2.0f), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 0.0f, 1.0f));
    ubo.proj = glm::perspective(glm::radians(45.0f), (float)TEX_WIDTH / (float) TEX_HEIGHT, 0.1f, 10.0f);

    void* data;
    vkMapMemory(vk_core.dev, vk_ubo.mem, 0, sizeof(UniformBufferObject), 0, &data);
    memcpy(data, &ubo, sizeof(UniformBufferObject));
    vkUnmapMemory(vk_core.dev, vk_ubo.mem);
}

int main(int, char**)
{
    if (!glfwInit())
        return -1;

    opengl_init();

    glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
    glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);

    GLFWwindow* window = glfwCreateWindow(1280, 960, "Vulkan", nullptr, nullptr);
    if (!window) {
        fprintf(stderr, "failed to create windown\n");
        return -1;
    }

    vk_init(window);

    gl_create_mem_obj_from_vk_mem(&vk_core, &vtx_buf.mem, vtx_buf.size, &gl_data.gl_vtx_mem);
    gl_gen_buf_from_mem_obj(gl_data.gl_vtx_mem, vtx_buf.size, &gl_data.gl_vtx);

    gl_create_semaphores_from_vk(&vk_core, &vk_sem, &gl_data.gl_sem);
    opengl_setup();

    unsigned img_idx;

    vk_build_draw_cmd(&vk_core, &vk_vtx, &vk_idx, NULL, &vk_render, vk_fb_color, 4, TEX_WIDTH, TEX_HEIGHT);

    while(!glfwWindowShouldClose(window)) {
        glfwPollEvents();

        vk_acquire(&vk_core, &vk_sem, &img_idx);
        opengl_draw();
        animate();
        vk_set_image_layout(&vk_core, &vk_img.obj, NULL, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
        vk_submit(&vk_core, &vk_sem);
        vk_present(&vk_core, &vk_sem, img_idx);
    }

    return 0;
}
