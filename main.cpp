#include "imgui.h"
#include "imgui_impl_glfw.h"
#include "imgui_impl_opengl3.h"
#include <stdio.h>
#include <time.h>
#include <sys/time.h>
#include <iostream>
#include <vector>

#include <GL/gl3w.h>            // Initialize with gl3wInit()

// Include glfw3.h after our OpenGL definitions
#include <GLFW/glfw3.h>

// for GL extension
#include <GL/glext.h>

// For vulkan
#include <vulkan/vulkan.h>
#include <glm/glm.hpp>

#include "vk/vk.h"
#include "vk/interop.h"

#define TEX_WIDTH 160
#define TEX_HEIGHT 160

static void glfw_error_callback(int error, const char* description)
{
    fprintf(stderr, "Glfw Error %d: %s\n", error, description);
}

static void
imgui_init(GLFWwindow *window)
{
    const char* glsl_version = "#version 130";
    // Setup Dear ImGui context
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGuiIO& io = ImGui::GetIO(); (void)io;

    // Setup Dear ImGui style
    ImGui::StyleColorsClassic();

    // Setup Platform/Renderer bindings
    ImGui_ImplGlfw_InitForOpenGL(window, true);
    ImGui_ImplOpenGL3_Init(glsl_version);
}

static void
imgui_render()
{
    ImGui_ImplOpenGL3_NewFrame();
    ImGui_ImplGlfw_NewFrame();
    ImGui::NewFrame();

    ImGui::Begin("OpenGL Vulkan interoperability");
    ImGui::Text("GPU : %s", glGetString(GL_RENDERER));
    ImGui::Text("Avr: %.3f ms/frame (%.1f FPS)",
                1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
    ImGui::End();

    ImGui::Render();
}

static void
imgui_drawdata()
{
    ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
}

static void
imgui_destroy()
{
    // Cleanup
    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplGlfw_Shutdown();
    ImGui::DestroyContext();
}

struct gl_data {
    GLuint gl_tex;
    GLuint gl_vtx;
    GLuint gl_vtx_mem;
    GLuint gl_vao;
    struct gl_ext_semaphores gl_sem;
};

static struct gl_data gl_data = {0, };

static struct vk_ctx vk_core;
static struct vk_image_att vk_img;
static struct vk_image_att vk_depth_img;
static struct vk_descriptor_layout vk_desc;
static struct vk_semaphores vk_sem;
static struct vk_renderer vk_render;
static struct vk_buf vtx_buf;
static float  vk_fb_color[4] = { 1.0, 1.0, 1.0, 1.0 };


struct Vertex {
    glm::vec3 pos;
    glm::vec2 uv;
};

static std::vector<Vertex> g_vertex_data = {{{-0.5f,  0.5f, 0.0f}, {0.0, 0.0}},
                                             {{ 0.5f,  0.5f, 0.0f}, {1.0, 0.0}},
                                             {{-0.5f, -0.5f, 0.0f}, {0.0, 1.0}},
                                             {{ 0.5f, -0.5f, 0.0f}, {1.0, 1.0}}};

static
void vk_init()
{
    if (!vk_init_ctx_for_rendering(&vk_core, NULL, 0))
        fprintf(stderr, "Failed to initialize vulkan\n");

    vk_create_buffer(&vk_core,
                     sizeof(g_vertex_data[0]) * g_vertex_data.size(),
                     VK_BUFFER_USAGE_VERTEX_BUFFER_BIT, &vtx_buf);
    printf("Created vertex buffer in VK: size(%lu)\n", vtx_buf.size);

    vk_create_semaphores(&vk_core, &vk_sem);
}

static void
vk_renderer()
{
    uint32_t num_samples = 1;
    uint32_t num_levels = 1;
    uint32_t num_layers = 1;
    VkFormat color_format = VK_FORMAT_R32G32B32A32_SFLOAT;
    VkFormat depth_format = VK_FORMAT_D32_SFLOAT;
    VkImageUsageFlags color_usage = VK_IMAGE_USAGE_SAMPLED_BIT |
                                    VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
    VkImageUsageFlags depth_usage = VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT;
    VkImageTiling color_tiling = VK_IMAGE_TILING_LINEAR;
    VkImageTiling depth_tiling = VK_IMAGE_TILING_OPTIMAL;
    VkImageLayout color_in_layout = VK_IMAGE_LAYOUT_UNDEFINED;
    VkImageLayout color_end_layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
    VkImageLayout depth_in_layout = VK_IMAGE_LAYOUT_UNDEFINED;
    VkImageLayout depth_end_layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

    vk_fill_ext_image_props(&vk_core, TEX_WIDTH, TEX_HEIGHT, 1,
                            num_samples, num_levels, num_layers,
                            color_format, color_tiling, color_usage,
                            color_in_layout, color_end_layout, &vk_img.props);

    vk_create_ext_image(&vk_core, &vk_img.props, &vk_img.obj);

    vk_fill_ext_image_props(&vk_core, TEX_WIDTH, TEX_HEIGHT, 1,
                            num_samples, num_levels, num_layers,
                            depth_format, depth_tiling, depth_usage,
                            depth_in_layout, depth_end_layout, &vk_depth_img.props);

    vk_create_ext_image(&vk_core, &vk_depth_img.props, &vk_depth_img.obj);

    /* create Vulkan renderer */
    if (!vk_create_renderer(&vk_core,
                            "shaders/vk_bands.vert.spv",
                            "shaders/vk_bands.frag.spv",
                            &vk_img, &vk_depth_img, &vk_render)) {
        fprintf(stderr, "Failed to create Vulkan renderer.\n");
        return;
    }
}

GLchar const* vertexShaderSource = {R"(
    #version 130
    in vec3 pos;
    in vec2 in_uv;
    out vec2 uv;

    void main()
    {
        gl_Position = vec4(pos, 1.0);
        uv = in_uv;
    }
)"};

GLchar const* fragmentShaderSource = {R"(
    #version 130
    in vec2 uv;
    uniform sampler2D tex;
    out vec4 color;
    void main()
    {
        color = texture(tex, uv);
    }
)"};

static
void opengl_setup()
{
    // build and compile our shader program
    // ------------------------------------
    // vertex shader
    int vertexShader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertexShader, 1, &vertexShaderSource, NULL);
    glCompileShader(vertexShader);
    // check for shader compile errors
    int success;
    char infoLog[512];
    glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &success);
    if (!success)
    {
        glGetShaderInfoLog(vertexShader, 512, NULL, infoLog);
        std::cout << "ERROR::SHADER::VERTEX::COMPILATION_FAILED\n" << infoLog << std::endl;
    }
    // fragment shader
    int fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragmentShader, 1, &fragmentShaderSource, NULL);
    glCompileShader(fragmentShader);
    // check for shader compile errors
    glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &success);
    if (!success)
    {
        glGetShaderInfoLog(fragmentShader, 512, NULL, infoLog);
        std::cout << "ERROR::SHADER::FRAGMENT::COMPILATION_FAILED\n" << infoLog << std::endl;
    }
    // link shaders
    int shaderProgram = glCreateProgram();
    glAttachShader(shaderProgram, vertexShader);
    glAttachShader(shaderProgram, fragmentShader);

    glLinkProgram(shaderProgram);
    // check for linking errors
    glGetProgramiv(shaderProgram, GL_LINK_STATUS, &success);
    if (!success) {
        glGetProgramInfoLog(shaderProgram, 512, NULL, infoLog);
        std::cout << "ERROR::SHADER::PROGRAM::LINKING_FAILED\n" << infoLog << std::endl;
    }
    glDeleteShader(vertexShader);
    glDeleteShader(fragmentShader);

    glGenVertexArrays(1, &gl_data.gl_vao);
    glBindVertexArray(gl_data.gl_vao);

    /* Binding vetex array */
    glVertexAttribFormat(0, 3, GL_FLOAT, GL_FALSE, 0);
    glVertexAttribBinding(0, 0);
    glEnableVertexAttribArray(0);

    glVertexAttribFormat(1, 2, GL_FLOAT, GL_FALSE, offsetof(Vertex, uv));
    glVertexAttribBinding(1, 0);
    glEnableVertexAttribArray(1);

    glBindVertexBuffer(0, gl_data.gl_vtx, 0, sizeof(Vertex));

    /* Bind shared texture from VK */
    GLuint gl_mem_obj;
    gl_create_mem_obj_from_vk_mem(&vk_core, &vk_img.obj.mem, vk_img.obj.mem_sz, &gl_mem_obj);
    gl_gen_tex_from_mem_obj(&vk_img.props, GL_RGBA32F, gl_mem_obj, 0, &gl_data.gl_tex);

    printf("Shared texture ID: %d\n", gl_data.gl_tex);

    glUseProgram(shaderProgram);
}

static void
animate()
{
    static bool reverse = false;
    float diff = 0.006f;

    if (g_vertex_data[0].pos.y < 0)
        reverse = true;
    else if (g_vertex_data[0].pos.y > 0.5)
        reverse = false;

    if (!reverse) {
        g_vertex_data[0].pos.y -= diff;
        g_vertex_data[1].pos.x -= diff;
        g_vertex_data[1].pos.y -= diff;
        g_vertex_data[3].pos.x -= diff;
    } else {
        g_vertex_data[0].pos.y += diff;
        g_vertex_data[1].pos.x += diff;
        g_vertex_data[1].pos.y += diff;
        g_vertex_data[3].pos.x += diff;
    }

    void *data;
    vkMapMemory(vk_core.dev, vtx_buf.mem, 0, vtx_buf.size, 0, &data);
    memcpy(data, g_vertex_data.data(), g_vertex_data.size() * sizeof(Vertex));
    vkUnmapMemory(vk_core.dev, vtx_buf.mem);
}

static void
opengl_draw(GLFWwindow *window)
{
    glClearColor(0.45f, 0.55f, 0.60f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);

    glBindTexture(GL_TEXTURE_2D, gl_data.gl_tex);

    // render the quad
    glBindVertexArray(gl_data.gl_vao);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
}

int main(int, char**)
{
    // Setup window
    glfwSetErrorCallback(glfw_error_callback);
    if (!glfwInit())
        return 1;

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);

    // Create window with graphics context
    GLFWwindow* window = glfwCreateWindow(1280, 720, "GL-VK Interop example example", NULL, NULL);
    if (window == NULL)
        return 1;
    glfwMakeContextCurrent(window);

    // Initialize OpenGL loader
    bool err = gl3wInit() != 0;
    if (err)
    {
        fprintf(stderr, "Failed to initialize OpenGL loader!\n");
        return 1;
    }

    imgui_init(window);

    vk_init();
    gl_create_mem_obj_from_vk_mem(&vk_core, &vtx_buf.mem, vtx_buf.size, &gl_data.gl_vtx_mem);
    gl_gen_buf_from_mem_obj(gl_data.gl_vtx_mem, vtx_buf.size, &gl_data.gl_vtx);

    vk_renderer();

    gl_create_semaphores_from_vk(&vk_core, &vk_sem, &gl_data.gl_sem);
    opengl_setup();

    PFN_glSignalSemaphoreEXT glSignalSemaphoreEXT =
       (PFN_glSignalSemaphoreEXT) glfwGetProcAddress ("glSignalSemaphoreEXT");
    assert (glSignalSemaphoreEXT != NULL);

    PFN_glSignalSemaphoreEXT glWaitSemaphoreEXT =
       (PFN_glWaitSemaphoreEXT) glfwGetProcAddress ("glWaitSemaphoreEXT");
    assert (glWaitSemaphoreEXT != NULL);

    GLenum layout = GL_NONE;
    struct vk_dims dims = { .w = TEX_WIDTH, .h = TEX_HEIGHT };
    vk_build_draw_cmd(&vk_core, NULL, NULL, &dims, &vk_render, vk_fb_color, 4, TEX_WIDTH, TEX_HEIGHT);

    // Main loop
    while (!glfwWindowShouldClose(window))
    {
        glfwPollEvents();
        imgui_render();
        animate();
        opengl_draw(window);
        imgui_drawdata();

        layout = GL_NONE;

        glSignalSemaphoreEXT(gl_data.gl_sem.gl_frame_done, 0, 0, 1, &gl_data.gl_tex, &layout);
        glFlush();

        vk_submit(&vk_core, &vk_sem);
        layout = GL_LAYOUT_COLOR_ATTACHMENT_EXT;
        glWaitSemaphoreEXT(gl_data.gl_sem.gl_frame_ready, 0, 0, 1, &gl_data.gl_tex, &layout);

        glfwSwapBuffers(window);
    }

    imgui_destroy();

    glfwDestroyWindow(window);
    glfwTerminate();

    return 0;
}
